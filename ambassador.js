const token = localStorage.getItem("token");
const disguise = localStorage.getItem("disguise");
const headers = {
  "Content-Type": "application/json",
  ...(token ? { token } : {}),
  ...(disguise ? { disguise } : {}),
};

const request = async ({detail}) {
  const { method = "GET", module = "", action = "", ...options } = detail;
  const body = ["POST", "PUT", "PATCH"].includes(method) ? options : {};
  const query = ["GET", "DELETE"].includes(method) ? options : {};
  const qs = new URLSearchParams();
  Object.keys(query).forEach(key => qs.append(key, query[key]));
  const qsString = qs.toString();
  const separator = qsString.length ? "?" : "";
  const uri = `${action}${separator}${qsString}`;
  const url = `${process.env.API_URL}/${uri}`;
  const response = await fetch(url, {
    headers,
    method,
    ...options,
    ...(body ? { body: JSON.stringify(body) } : {}),
  });
  const resEvName = `fetched:${method}:${module}:${action}`;
  const sucEvName = `${resEvName}:success`;
  const errEvName = `${resEvName}:error`;
  document.dispatchEvent(new CustomEvent(resEvName, { detail: response }));
  if (response.status >= 200 && response.status < 300) 
    document.dispatchEvent(new CustomEvent(sucEvName, { detail: response }));
  if (response.status >= 400 && response.status !== 503) 
    document.dispatchEvent(new CustomEvent(errEvName, { detail: response }));
}

export const watchAmbassador = () => {
  document.addEventListener("fetch", request)
}
