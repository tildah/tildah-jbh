const template = /* html */ `
  <style>
    :host {
      display: none;
    }
    
    :host[data-visible="true"] {
      display: block;
    }
  </style>
  <slot></slot>
`
const getParams = (pageRoute, route) => {
  const regex = new RegExp(pageRoute);
  const matches = regex.exec(route);
  [ "index", "input", "groups" ].forEach(k => delete matches[k]);
  matches.shift();
  return matches;
}

const getQS = (href) => {
  const url = new URL(href);
  const { search } = url;
  const qs = new URLSearchParams(search);
  const detail = {};
  qs.forEach((value, key) => {
    detail[key] = value;
  });
  return detail;
}

class RouteGuest extends HTMLElement {

  constructor(...args) {
    super(...args);
    this.attachShadow({ mode: "open" });
    this.onPopState = this.onPopState.bind(this);
  }

  connectedCallback() {
    this.render();
    this.watch();
  }

  render() {
    this.shadowRoot.innerHTML = "";
    const templateEl = document.createElement("template");
    templateEl.innerHTML = template;
    this.shadowRoot.appendChild(templateEl.content.cloneNode(true));
  }

  onPopState() {
    const { route, prefix = "route-guest" } = this.dataset;
    const { href } = window.location;
    const isConcerned = new RegExp(route).test(href);
    this.setAttribute("data-visible", isConcerned);
    if(!isConcerned) return;
    const params = getParams(route, href);
    const qs = getQS(href);
    const detail = { params, qs };
    const eventName = `${prefix}:changed`;
    document.dispatchEvent(new CustomEvent(eventName, { detail }));
  }

  get watchers() {
    return [
      { event: "popstate", listener: this.onPopState },
    ];
  }

  watch() {
    this.watchers.forEach(watcher => {
      window.addEventListener(watcher.event, watcher.listener);
    });
  }

  disconnectedCallback() {
    this.watchers.forEach(watcher => {
      window.removeEventListener(watcher.event, watcher.listener);
    });
  }
}

customElements.define("route-guest", RouteGuest);
