import nestedProperty from "nested-property";

export class EDFEDInput extends HTMLInputElement {

  constructor(...args) {
    super(...args);
    this.addEventListener("change", () => this.getDispatcher("change"));
    this.addEventListener("keyup", () => this.getDispatcher("keyup"));
    if (this.hasAttribute("hide")) this.style.display = "none";
    this.toBucket = this.toBucket.bind(this);
    this.alter = this.alter.bind(this);
    const loaderEvent = this.dataset.loader_event;
    if (loaderEvent) document.dispatchEvent(new CustomEvent(loaderEvent, {
      detail: this.dataset,
    }));
  }

  connectedCallback() {
    this.watch();
  }

  watch() {
    const alterOn = this.getAttribute("alter-on");
    if (alterOn) alterOn.split(";").forEach(event => {
      document.addEventListener(event, this.alter);
    });
    document.addEventListener(this.getAttribute("focus-on"), this.focus);
  }

  disconnectedCallback() {
    const alterOn = this.getAttribute("alter-on");
    if (alterOn) alterOn.split(";").forEach(event =>
      document.removeEventListener(event, this.alter));
    document.removeEventListener(this.getAttribute("focus-on"), this.focus);
  }

  toBucket() {
    const bucket = document.querySelector(this.dataset.bucket);
    nestedProperty.set(bucket, this.getAttribute("name"), this.value);
  }

  get alterMethod() {
    const { type } = this;
    const methodsNames = {
      text: this.alterDefault,
      checkbox: this.alterCheckbox,
    };
    return methodsNames[type] || this.alterDefault;
  }

  alterDefault(value) {
    this.value = value;
  }

  alterCheckbox(value) {
    this.checked = value;
  }

  alter(e) {
    const key = this.getAttribute("alter-key");
    this.alterMethod(nestedProperty.get(e.detail, key));
    this.getDispatcher("change");
  }

  getDispatcher(listenedEvent) {
    this.toBucket();
    const dispatchedEvent = this.getAttribute(`${listenedEvent}-call`);
    if (!dispatchedEvent) return;
    const sendAs = this.dataset.as || "value";
    document.dispatchEvent(new CustomEvent(dispatchedEvent, {
      detail: {
        [sendAs]: this.value,
        target: this,
        checked: this.checked,
        ...this.dataset,
      },
    }));
  }
}

customElements.define("edfed-input", EDFEDInput, { extends: "input" });
