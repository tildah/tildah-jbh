class CursorEvent extends HTMLElement {

  constructor() {
    super();
    this.dispatch = this.dispatch.bind(this);
    this.disable = this.disable.bind(this);
    this.enable = this.enable.bind(this);
  }

  connectedCallback() {
    this.setStyle();
    this.watch();
  }

  get eventsRange() {
    return [
      "click",
      "dblclick",
      "mouseenter",
      "mouseleave",
    ];
  }

  get events() {
    return this.eventsRange.filter(ev => this.dataset[`${ev}To`]);
  }

  get dispatchDetail() {
    const reserved = this.eventsRange.map(ev => `${ev}To`);
    const keys = Object.keys(this.dataset).filter(k => !reserved.includes(k));
    return keys.reduce((a, k) => ({ ...a, [k]: this.dataset[k] }), {});
  }

  setStyle() {
    if (this.dataset.clickTo) this.style.cursor = "pointer";
  }

  watch() {
    const { events } = this;
    const { disableOn, enableOn } = this.dataset;
    events.forEach(ev => {
      this.addEventListener(ev, this.dispatch);
    });
    if (disableOn) document.addEventListener(disableOn, this.disable);
    if (enableOn) document.addEventListener(enableOn, this.enable);
  }

  disconnectedCallback() {
    const { events } = this;
    const { disableOn, enableOn } = this.dataset;
    events.forEach(ev => {
      this.removeEventListener(ev, this.dispatch);
    });
    if (disableOn) document.removeEventListener(disableOn, this.disable);
    if (enableOn) document.removeEventListener(enableOn, this.enable);
  }

  disable() {
    this.disabled = true;
  }

  enable() {
    this.disabled = false;
  }

  dispatch(e) {
    if (this.disabled) return;
    const { type } = e;
    const to = this.dataset[`${type}To`];
    document.dispatchEvent(new CustomEvent(to, {
      detail: this.dispatchDetail,
    }));
    e.stopPropagation();
  }

}

customElements.define("cursor-event", CursorEvent);
