import translator from "translator.js";

class LangSpan extends HTMLElement {

  constructor() {
    super();
    this.onEvent = this.onEvent.bind(this);
    this.render();
    this.watch();
  }

  render() {
    this.text = this.dataset.text;
    if (this.text === undefined) return;
    const template = this.dataset.template || "%s";
    const translationKey = template.replace("%s", this.text);
    const content = translator.__(translationKey);
    this.innerHTML = content;
  }

  static get observedAttributes() {
    return [ "data-text" ];
  }

  attributeChangedCallback() {
    this.render();
  }


  onEvent(e) {
    this.setAttribute("data-text", e.detail[this.dataset.key]);
    this.render();
  }

  watch() {
    if (this.dataset.event)
      document.addEventListener(this.dataset.event, this.onEvent);
  }

  disconnectedCallback() {
    if (this.dataset.event)
      document.removeEventListener(this.dataset.event, this.onEvent);
  }

}

customElements.define("lang-span", LangSpan);
