import { colors } from "global-vars.js";

export default () => /* html */ `
  <style>
    :host {
      border: 1px solid ${colors.main};
      border-radius: 4px;
      margin: 8px;
      padding: 8px;
      box-sizing: border-box;
      background-color: white;
      display: flex;
    }

    .value-cell {
      flex: 1;
    }

    .title {
      color: rgba(0, 0, 0, .58);
    }

    .value {
      font-size: 32px;
      color: ${colors.main};
    }
  </style>
  <div class="value-cell">
    <div class="title"> Net à payer </div>
    <div class="value"> <slot name="value"></slot> DT </div>
    <slot name="discount"></slot>
  </div>
  <div class="mode"> <slot name="mode"></slot> </div>
  <div class="actions"> <slot name="actions"></slot> </div>
`;
