const template = /* html */ `
  <style>
    :host {
      display: flex;
      width: 100%;
      flex-wrap: wrap;
    }
    :host([train]) {
      justify-content: flex-start;
      align-items: center;
    }
    :host([column]) {
      flex-direction: column;
    }
    :host([col-center]) {
      flex-direction: column;
      justify-content: center;
      align-items: center;
    }
    :host([row-center]) {
      justify-content: center;
      align-items: center;
    }
    ::slotted([flex-initial]) {
      flex: initial;
    }
    ::slotted([flex-fill]) {
      flex: auto;
    }
  </style> <slot></slot> 
`;

class FlexBox extends HTMLElement {

  constructor(...args) {
    super(...args);
    this.attachShadow({ mode: "open" });
  }

  connectedCallback() {
    this.render();
  }

  render() {
    this.shadowRoot.innerHTML = "";
    const templateEl = document.createElement("template");
    templateEl.innerHTML = template;
    this.shadowRoot.appendChild(templateEl.content.cloneNode(true));
  }

}

customElements.define("flex-box", FlexBox);
