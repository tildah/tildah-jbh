import np from "nested-property";

export class ArrayInput extends HTMLInputElement {

  constructor(...args) {
    super(...args);
    this.style.display = "none";
    this._list = [];
    this.updateList = this.updateList.bind(this);
    this.watch();
  }

  watch() {
    const { listFrom } = this.dataset;
    if (listFrom) listFrom.split(";").forEach(event => {
      document.addEventListener(event, this.updateList);
    });
  }

  disconnectedCallback() {
    const { listFrom } = this.dataset;
    if (listFrom) listFrom.split(";").forEach(event => {
      document.removeEventListener(event, this.updateList);
    });
  }

  get value() {
    return this._list;
  }

  updateList(e) {
    const { fromKey = "", field = "" } = this.dataset;
    const complete = np.get(e.detail, fromKey) || [];
    this._list = complete.map(item => np.get(item, field));
    this.dispatch();
  }

  dispatch() {
    this.form.dispatchEvent(new Event("input"));
  }

}

customElements.define("array-input", ArrayInput, { extends: "input" });
