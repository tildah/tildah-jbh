import nestedProperty from "nested-property";

class AutocompleteList extends HTMLElement {

  constructor(...args) {
    super(...args);
    this.shadow = this.attachShadow({ mode: "open" });
    if (!this.id) throw new Error(this.noIDErrorMessage);
    this._list = [];
    this.watch();
  }

  watch() {
    document.addEventListener(this.getAttribute("loaded-event"), e => {
      const fromKey = this.getAttribute("from-key") || "";
      this._list = nestedProperty.get(e.detail, fromKey) || [];
      this.render();
    });
  }

  get noIDErrorMessage() {
    return "An autocomplete-list component should always have an id";
  }

  get showFormEvent() {
    return `${this.id}:show_form`;
  }

  get hideFormEvent() {
    return `${this.id}:hide_form`;
  }

  get formShownEvent() {
    return `${this.id}:form_shown`;
  }

  get itemSelectedEv() {
    return `${this.id}:item_selected`;
  }

  get styleTag() {
    return /* html */ `
      <style>
        .list {
          padding-top: 8px;
          position: absolute;
          bottom: 100%;
          left: 0;
          background-color: #fff;
          box-shadow: 2px 2px 8px rgba(0,0,0, .12);
          border: 1px solid rgba(0, 0, 0, .58);
          color: black;
          display: ${this._list.length ? "flex" : "none"};
          flex-direction: column;
          width: 100%;
        }

        .list-item {
          background-color: #fff;
          padding: 8px;
          cursor: pointer;
          border-bottom: 1px solid rgba(0, 0,0,.12);
          min-height: 36px;
        }

        .list-item:hover {
          background-color: rgba(0,0,0, .12);
        }
      </style>
   `;
  }

  get body() {
    return this._list.map((item, index) => /* html*/ `
      <div 
        class="list-item" 
        onclick="${this.id}.selectItem(this)"
        data-index="${index}"
        >
        ${this.renderRow(item)}
      </div>
    `).join("");
  }

  get template() {
    return /* html*/ `
      ${this.styleTag}
      <div class="list">
        ${this.body}
      </div>
    `;
  }

  simpleCell(item) {
    return /* html*/ `
      ${item[this.dataset.default_display_key]}
    `;
  }

  renderRow(item) {
    const template = this.querySelector("template");
    if (!template) return this.simpleCell(item);
    const rowContent = document.createElement("div");
    rowContent.appendChild(template.content.cloneNode(true));
    Object.keys(item).forEach(key => {
      const keyExp = new RegExp(`\\$\\[${key}\\]`, "g");
      rowContent.innerHTML = rowContent.innerHTML.replace(keyExp, item[key]);
    });

    return rowContent.innerHTML;
  }

  connectedCallback() {
    this.render();
  }

  render() {
    this.shadow.innerHTML = "";
    const templateEl = document.createElement("template");
    templateEl.innerHTML = this.template;
    this.shadow.appendChild(templateEl.content.cloneNode(true));
  }

  selectItem(itemElement) {
    this.selectedIndex = parseInt(itemElement.dataset.index);
    const item = this._list[this.selectedIndex];
    document.dispatchEvent(new CustomEvent(this.itemSelectedEv, {
      detail: item,
    }));
    this.render();
  }

}

customElements.define("autocomplete-list", AutocompleteList);
