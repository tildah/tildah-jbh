class EventEvent extends HTMLElement {

  constructor(...args) {
    super(...args);
    this.attachShadow({ mode: "open" });
    this.dispatch = this.dispatch.bind(this);
  }

  get template() {
    return /* html*/ `
      <style>
        :host {
          display: none;
        }
      </style>
      <slot></slot>
    `;
  }

  connectedCallback() {
    this.remake = {};
    this.initInducers();
    this.render();
    this.watch();
  }

  render() {
    this.shadowRoot.innerHTML = "";
    const templateEl = document.createElement("template");
    templateEl.innerHTML = this.template;
    this.shadowRoot.appendChild(templateEl.content.cloneNode(true));
  }

  initInducers() {
    const { on } = this.dataset;
    this.inducers = on.split(",").map(i => ({ name: i, run: 0 }));
  }

  watch() {
    this.inducers.forEach(inducer => {
      document.addEventListener(inducer.name, this.dispatch);
    });
  }

  disconnectedCallback() {
    this.inducers.forEach(inducer => {
      document.removeEventListener(inducer.name, this.dispatch);
    });
  }

  parseRemake(detail, remake) {
    if (remake === "*") return detail;
    const props = remake.split(",").map(prop => prop.split(":"));
    return props.reduce((result, [ from, to ]) => ({
      ...result,
      ...((detail || {})[from] == null ? {} : { [to]: detail[from] } ),
    }), {});
  }

  get isReadyToSend() {
    switch (this.dataset.dispatchMode) {
    case "independent":
      return true;
      break;
    default:
      return this.inducers.every(i => i.run);
    }
  }

  dispatch(e) {
    const { detail, type } = e;
    this.inducers.find(i => i.name === type).run++;
    const {
      to,
      on,
      dispatchMode,
      mode = "normal",
      as = "value",
      remake = "",
      delay = "0",
      ...options
    } = this.dataset;
    if (mode !== "gather") this.remake = {};
    this.remake = { ...this.remake, ...this.parseRemake(detail, remake) };
    if (!to) return;
    const slot = this.shadowRoot.querySelector("slot");
    const content = this.getContent(slot);
    const timeout = parseInt(delay);
    if (this.isReadyToSend) setTimeout( () => {
      document.dispatchEvent(new CustomEvent(this.dataset.to, { detail: {
        ...(content ? { [as]: content } : {}), ...options, ...this.remake,
      } }));
    }, timeout );
  }

  getContent(slot) {
    const assigneds = slot.assignedElements();
    const [ el ] = assigneds;
    if (!el) return;
    return el.tagName === "SLOT" ? this.getContent(el) : el.textContent;
  }

}

customElements.define("event-event", EventEvent);
