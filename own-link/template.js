import "mdi-component/mdi-component.js";
import { colors } from "global-vars.js";

const textColors = {
  red: colors.red,
};

export default component => /* html */ `
  <style>
    :host {
      margin: 4px;
      color: ${textColors[component.dataset.color] || colors.main};
      cursor: pointer;
      text-decoration: none;
    }
    .content {
      display: inline-flex;
      align-items: center;
    }
    .content:hover {
      text-decoration: underline;
    }
    .text {
      display: none;
    }
    @media only screen and (min-width: 768px) {
      .text {
        display: inline;
      }
    }
  </style>
  <span class="content">
    ${component.dataset.icon === undefined ? "" : /* html */ `
      <mdi-component size="24px" path="${component.dataset.icon}">
      </mdi-component>
    `}
    <span class="text"> <slot></slot> </span>
  </span>
`;
