import "gen_components/event-guest.js";
import "own_components/barcode-scanner/barcode-scanner.js";

export default component => /* html */ `
  <style>
    .barcode-popup {
      display: flex;
      justify-content: center; 
      align-items: center;
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      max-width: 100%;
      height: 100vh;
      background-color: rgba(0, 0, 0, .6);
      z-index: 5;
    }

    .barcode-cambox {
      height: 500px;
      width: 90%;
    }

    @media only screen and (min-width: 768px) {
      .barcode-cambox {
        width: 500px;
      }
    }
  </style>
  <event-guest
    showon="${component.dataset.scannerId}:camera:show"
    hideon="${component.dataset.scannerId}:camera:hide">
    <div class="barcode-popup">
      <div class="barcode-cambox">
        <barcode-scanner data-as="${component.dataset.as || "value"}" data-event="${component.dataset.to}"></barcode-scanner>
      </div>
    </div>
  </event-guest>
`;
