import "mdi-component/mdi-component.js";

export default component => /* html */ `
  <style>
    :host {
      display: flex;
      flex-direction: row;
      align-items: center;
      height: 40px;
      padding: 0 16px;
      font-size: 14px;
      text-transform: capitalize;
      color: rgba(0, 0, 0, .84);
      text-decoration: none;
    }

    :host(:hover) {
      cursor: pointer;
      background-color: #f5f5f5;
    }

    .icon {
      font-size: 20px;
      width: 36px;
      color: rgba(0, 0, 0, .38);
    }
  </style>
  <div class="icon">
    <mdi-component path="${component.dataset.icon}"></mdi-component>
  </div>
  <slot></slot>
`;
