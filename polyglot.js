import locales from "locales/index.js";

class Polyglot {

  constructor(settings) {
    /*
      Properties:
        storageKey  : Key name in localstorage where current lang is stored
        setLangEvent: Event name to set new lang in storage
        setLangFrom : Key to take lang from in event. if not given, taken
                      directly from detail
    */
    Object.assign(this, settings);
    this.lang = localStorage.getItem(this.storageKey);
    this.list = Object.keys(locales);
    this.defaultLang = this.list[0];
    this.locale = locales[this.lang || this.defaultLang];
    this.watch();
  }

  __(str) {
    return this.locale[str] || str;
  }

  watch() {
    document.addEventListener(this.setLangEvent, e => {
      const newLang = this.setLangFrom ? e.detail[this.setLangFrom] : e.detail;
      localStorage.setItem(this.storageKey, newLang);
      window.location.reload();
    });
  }

}

export default Polyglot;
