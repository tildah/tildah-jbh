class EventGuest extends HTMLElement {

  constructor() {
    super();
    const computedStyle = window.getComputedStyle(this);
    this.initialDisplay = computedStyle.getPropertyValue("display");
    if (!this.hasAttribute("start-visible")) this.style.display = "none";
    this.show = this.show.bind(this);
    this.hide = this.hide.bind(this);
    this.watch();
  }

  watch() {
    const showOn = this.getAttribute("showon");
    const hideOn = this.getAttribute("hideon");
    if (showOn){
      const showOnEvents = showOn.split(";");
      showOnEvents.forEach(event => {
        document.addEventListener(event, this.show);
      });
    }
    if (hideOn){
      const hideOnEvents = hideOn.split(";");
      hideOnEvents.forEach(event => {
        document.addEventListener(event, this.hide);
      });

    }
  }

  disconnectedCallback() {
    const showOn = this.getAttribute("showon");
    const hideOn = this.getAttribute("hideon");
    if (showOn){
      const showOnEvents = showOn.split(";");
      showOnEvents.forEach(event => {
        document.removeEventListener(event, this.show);
      });
    }
    if (hideOn){
      const hideOnEvents = hideOn.split(";");
      hideOnEvents.forEach(event => {
        document.removeEventListener(event, this.hide);
      });
    }
  }

  show() {
    this.style.display = this.initialDisplay;
    const shownEvent = this.getAttribute("shownEvent");
    if (!shownEvent) return;
    document.dispatchEvent(new CustomEvent(shownEvent));
  }

  hide() {
    this.style.display = "none";
  }

}

customElements.define("event-guest", EventGuest);
