export default component => /* html */ `
  <style>
  :host {
    position: relative;
    display: inline-block;
  }

  .text {
    width: 0;
    height: 0;
    white-space: nowrap;
    overflow: hidden;
    background-color: #555;
    color: #fff;
    border-radius: 6px;
    padding: 4px;
    margin: 4px 0;
    position: absolute;
    z-index: 4;
    bottom: 100%;
    right: 0;
    font-size: 16px;
    opacity: 0;
  }

  .text::after {
    content: "";
    position: absolute;
    top: 100%;
    right: 8px;
    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: #555 transparent transparent transparent;
  }

  :host(:hover) .text {
    visibility: visible;
    width: auto;
    height: auto;
    opacity: 1;
    transition: opacity 0.8s;
    transition-delay: 1s;
    -webkit-transition: opacity 0.8s;
    -webkit-transition-delay: 1s;
  }
  </style>
  <span class="text">${component.dataset.text || ""}</span>
  <slot></slot>
`;
