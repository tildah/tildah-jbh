import "gen_components/cursor-event.js";
import nestedProperty from "nested-property";

class MapList extends HTMLElement {

  constructor(...args) {
    super(...args);
    if (!this.id) throw new Error(this.noIDErrorMessage);
    this._list = [];
    this.onLoaded = this.onLoaded.bind(this);
    this.onPush = this.onPush.bind(this);
    this.dispatchItem = this.dispatchItem.bind(this);
    this.resetList = this.resetList.bind(this);
    this.onStrew = this.onStrew.bind(this);
  }

  get loadedEvent() {
    return this.dataset.loadedEvent || `${this.id}:loaded-event`;
  }

  get itemCursorEvents() {
    const events = [ "mouseenter", "mouseleave", "click" ];
    const capitalize = s => s.charAt(0).toUpperCase() + s.substr(1);
    return events.map(ev => ({
      attr: `data-${ev}-to`,
      dispatchAs: this.dataset[`item${capitalize(ev)}To`],
      getAs: this.cursorEvName(ev),
    })).filter(ev => ev.dispatchAs);
  }

  watch() {
    const { loadedEvent, onLoaded, onPush, resetList, onStrew } = this;
    const { pushFrom, resetOn, strewOn } = this.dataset;

    document.addEventListener(loadedEvent, onLoaded);
    if (pushFrom) document.addEventListener(pushFrom, onPush);
    if (resetOn) document.addEventListener(resetOn, resetList);
    if (strewOn) document.addEventListener(strewOn, onStrew);
    this.itemCursorEvents.forEach(ev => {
      document.addEventListener(ev.getAs, this.dispatchItem);
    });
  }

  disconnectedCallback() {
    const { loadedEvent, onLoaded, onPush, resetList, onStrew } = this;
    const { pushFrom, resetOn, strewOn } = this.dataset;

    document.removeEventListener(loadedEvent, onLoaded);
    if (pushFrom) document.removeEventListener(pushFrom, onPush);
    if (resetOn) document.removeEventListener(resetOn, resetList);
    if (strewOn) document.removeEventListener(strewOn, onStrew);
    this.itemCursorEvents.forEach(ev => {
      document.removeEventListener(ev.getAs, this.dispatchItem);
    });
  }

  onStrew({ detail: { strewTo } }) {
    if (!strewTo) throw new Error("can't strew list, `strewTo` is not defined");
    document.dispatchEvent(new CustomEvent(strewTo, { detail: this._list }));
  }

  resetList() {
    this._list = [];
    this.render();
  }

  onLoaded(e) {
    const { detail } = e;
    const { fromKey = "", mode = "replace" } = this.dataset;
    const loadedList = nestedProperty.get(detail, fromKey) || [];
    const modesNewList = {
      append: () => [ ...this._list, ...loadedList ],
      replace: () => loadedList,
    };
    this._list = modesNewList[mode]();
    this.render();
  }

  onPush(e) {
    const { pushFromKey = "" } = this.dataset;
    const item = nestedProperty.get(e.detail, pushFromKey);
    if (!item) return;
    this._list.push(item);
    this.render();
    this.dispatchPushed(item);
  }

  get loaderEventData() {
    const {
      loadedEvent,
      loaderEvent,
      onLoaded,
      resetOn,
      fromKey,
      clkMode,
      linkTemp,
      linkProp,
      linkIs,
      itemMouseenterTo,
      itemMouseleaveTo,
      itemClickTo,
      itemId,
      pushFrom,
      pushFromKey,
      pushedEvent,
      template,
      host,
      mode,
      strewOn,
      strewTo,
      ...data
    } = this.dataset;
    return data;
  }

  dispatchLoad() {
    const { loaderEvent } = this.dataset;
    if (!loaderEvent) return;
    document.dispatchEvent(new CustomEvent(loaderEvent, {
      detail: this.loaderEventData,
    }));
  }

  get noIDErrorMessage() {
    return "A map-list component should always have an id";
  }

  get emptySlot() {
    return this.dataset.emptySlot || "";
  }

  get template() {
    try {
      return this.getRootNode().getElementById(this.dataset.template);
    } catch (e) {
      return "";
    }
  }

  get host() {
    try {
      return this.getRootNode().getElementById(this.dataset.host) || this;
    } catch (e) {
      return this;
    }
  }

  cursorEvName(type) {
    return `${this.id}:item:${type}`;
  }

  eventItem(item, $index) {
    const cursorEvent = document.createElement("cursor-event");
    cursorEvent.style.display = "contents";
    this.itemCursorEvents.forEach(ev => {
      cursorEvent.setAttribute(ev.attr, ev.getAs);
    });
    cursorEvent.setAttribute("data-index", $index);
    cursorEvent.appendChild(this.renderItem(item, $index));
    return cursorEvent;
  }

  renderItem(rawItem, $index) {
    const { template } = this;
    const item = { ...rawItem, $index };
    const clone = template.content.cloneNode(true);
    const slots = clone.querySelectorAll("[data-slot]");
    slots.forEach(slot => {
      const content = nestedProperty.get(item, slot.dataset.slot);
      slot.textContent = content || this.emptySlot;
    });
    const patcheds = clone.querySelectorAll("[data-patch]");
    patcheds.forEach(patched => {
      const map = patched.dataset.patch.split(",").map(attr => attr.split(":"));
      map.forEach(([ key, as ]) => {
        const attrName = as || key;
        const value = nestedProperty.get(item, key);
        const tempAttrName = `${attrName}-temp`;
        const patchTemp = patched.getAttribute(tempAttrName) || "%s";
        const attValue = patchTemp.replace(/%s/, value);
        patched.setAttribute(attrName, attValue);
        patched.removeAttribute(tempAttrName);
      });
      patched.removeAttribute("data-patch");
    });
    return clone;
  }

  linkItem(item, $index) {
    const { linkIs } = this.dataset;
    const options = linkIs ? { is: linkIs } : {};
    const link = document.createElement("a", options);
    const template = this.dataset.linkTemp || "%s";
    const href = template.replace(/%s/, item[this.dataset.linkProp]);
    link.setAttribute("href", href);
    link.style.display = "contents";
    link.className = "item-link";
    link.appendChild(this.eventItem(item, $index));
    return link;
  }

  getInitialHTML() {
    const { mode } = this.dataset;
    const modesContents = { append: this.host.innerHTML };
    return modesContents[mode] || "";
  }

  connectedCallback() {
    this.initialHTML = this.getInitialHTML();
    this.watch();
    this.dispatchLoad();
  }

  get renderMethod() {
    const { clkMode } = this.dataset;
    const clkModeMethods = { a: this.linkItem, event: this.eventItem };
    return clkModeMethods[clkMode] || this.renderItem;
  }

  render() {
    const { host } = this;
    host.innerHTML = this.initialHTML;
    this._list.forEach((item, $index) => {
      const { renderMethod } = this;
      host.appendChild(renderMethod.bind(this)(item, $index));
    });
  }

  dispatchPushed(item) {
    const { pushedEvent } = this.dataset;
    if (!pushedEvent) return;
    document.dispatchEvent(new CustomEvent(pushedEvent, {
      detail: item,
    }));
  }

  dispatchItem({ type, detail }) {
    const { index } = detail;
    const item = this._list[index];
    const { dispatchAs } = this.itemCursorEvents.find(ev => ev.getAs === type);
    const eventName = dispatchAs.replace("%s", item[this.dataset.itemId]);
    document.dispatchEvent(new CustomEvent(eventName, { detail: item }));
  }

}

customElements.define("map-list", MapList);
