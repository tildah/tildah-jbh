class AutoDispatcher extends HTMLElement {

  constructor(...args) {
    super(...args);
    this.attachShadow({ mode: "open" });
  }

  get template() {
    return /* html*/ `
      <style>
        :host {
          display: none;
        }
      </style>
      <slot></slot>
    `;
  }

  connectedCallback() {
    this.render();
    this.dispatch();
  }

  render() {
    this.shadowRoot.innerHTML = "";
    const templateEl = document.createElement("template");
    templateEl.innerHTML = this.template;
    this.shadowRoot.appendChild(templateEl.content.cloneNode(true));
  }

  dispatch() {
    const { to, as = "value", ...options } = this.dataset;
    if (!to) return;
    const slot = this.shadowRoot.querySelector("slot");
    const content = this.getContent(slot);
    document.dispatchEvent(new CustomEvent(this.dataset.to, {
      detail: { ...(content ? { [as]: content } : {}), ...options },
    }));
  }

  getContent(slot) {
    const assigneds = slot.assignedElements();
    const [ el ] = assigneds;
    if (!el) return;
    return el.tagName === "SLOT" ? this.getContent(el) : el.textContent;
  }

}

customElements.define("auto-dispatcher", AutoDispatcher);
