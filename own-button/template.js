import { colors } from "global-vars.js";
import "own_components/loading-dots/loading-dots.js";
import "gen_components/event-guest.js";

export default component => /* html */ `
  <style>
    .content {
      display: ${component.hasAttribute("inline") ? "inline-flex" : "flex"};
      align-items: center;
      justify-content: center;
      margin: 4px;
      padding: 4px;
      background-color: ${colors.main};
      background-image: linear-gradient(rgba(255, 255, 255, .14), rgba(0,0,0,.12));
      color: white;
      border-radius: 4px;
      height: 32px;
      text-transform: uppercase;
      font-size: 14px;
      cursor: pointer;
      box-shadow: 0 2px 2px rgba(0, 0, 0, .54);
    }

    .content.accent {
      background-color: ${colors.accent};
    }

    .content.green {
      background-color: ${colors.green};
    }

    .content.red {
      background-color: ${colors.red};
    }

    .content.secondary {
      background-color: white;
      background-image: none;
      border: 1px solid ${colors.txtPrimary};
      color: ${colors.txtPrimary};
    }

    .content:active {
      box-shadow: none;
    }

    .event-guest {
      display: contents;
    }

    .icon {
      margin: 4px;
    }

    .icon + .text {
      margin: 4px;
      display: none;
    }

    @media only screen and (min-width: 768px) {
      .icon + .text {
        display: block;
      }
    }

  </style>
  <div class="content ${component.dataset.type}">
    <event-guest
      class="event-guest"
      start-visible
      hideon="${component.dataset.loadingOn || ""}"
      showon="${component.dataset.loadedOn || ""}"
    >
      ${component.dataset.icon ? /* html */ `
        <mdi-component size="24px" path="${component.dataset.icon}" class="icon"></mdi-component>
      ` : ""}
      <span class="text">${component.dataset.text}</span>
    </event-guest>
    <event-guest
      class="event-guest"
      showon="${component.dataset.loadingOn || ""}"
      hideon="${component.dataset.loadedOn || ""}"
    >
      <loading-dots data-color="white"></loading-dots>
    </event-guest>
  </div>
`;
