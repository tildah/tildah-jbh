export class EDFEDSelect extends HTMLSelectElement {

  constructor(...args) {
    super(...args);
    this.focus = this.focus.bind(this);
    this.alter = this.alter.bind(this);
  }

  connectedCallback() {
    this.watch();
    this.addEventListener("change", () => this.getDispatcher("change"));
    this.addEventListener("keyup", () => this.getDispatcher("keyup"));
    if (this.hasAttribute("hide")) this.style.display = "none";
    const loaderEvent = this.dataset.loader_event;
    if (loaderEvent) document.dispatchedEvent(new CustomEvent(loaderEvent, {
      detail: this.dataset,
    }));
  }

  watch() {
    const alterOn = this.getAttribute("alter-on");
    if (alterOn) alterOn.split(";").forEach(event => {
      document.addEventListener(event, this.alter);
    });
    const optionsFrom = this.getAttribute("options-from");
    document.addEventListener(this.getAttribute("focus-on"), this.focus);
    document.addEventListener(optionsFrom, this.updateOptions);
  }

  disconnectedCallback() {
    const alterOn = this.getAttribute("alter-on");
    if (alterOn) alterOn.split(";").forEach(event => {
      document.removeEventListener(event, this.alter);
    });
    const optionsFrom = this.getAttribute("options-from");
    document.removeEventListener(this.getAttribute("focus-on"), this.focus);
    document.removeEventListener(optionsFrom, this.updateOptions);
  }

  updateOptions(e) {
    const list = e.detail;
    this.innerHTML = "";
    list.forEach(item => {
      const op = document.createElement("option");
      op.value = item.value;
      op.textContent = item.label;
      op.selected = item.selected;
      this.appendChild(op);
    });
  }

  alter(e) {
    const key = this.getAttribute("alter-key");
    const list = Array.from(this.getElementsByTagName("option"));
    list.forEach(option => {
      option.selected = false;
    });
    const item = list.find(option => option.value === e.detail[key]);
    if (!item) return;
    item.selected = true;
    this.getDispatcher("change");
  }

  getDispatcher(listenedEvent) {
    const dispatchedEvent = this.getAttribute(`${listenedEvent}-call`);
    if (!dispatchedEvent) return;
    const sendAs = this.dataset.as || "value";
    document.dispatchEvent(new CustomEvent(dispatchedEvent, {
      detail: { [sendAs]: this.value, target: this },
    }));
  }
}

customElements.define("edfed-select", EDFEDSelect, { extends: "select" });
