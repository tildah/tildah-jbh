import { define } from "services/sahel.js";
import template from "./template.js";

const RadioButton = { as: "radio-button" };
RadioButton.inheritFrom = HTMLElement;

const onChange = compo => e => {
  const { value } = compo.element.dataset;
  if (e.detail.value === value) compo.element.setAttribute("active", "true");
  else compo.element.removeAttribute("active");
};

const listeners = compo => {
  const containerId = compo.element.parentNode.id;
  return [
    { event: `${containerId}:change`, fn: onChange },
  ];
};

RadioButton.template = template;
RadioButton.listeners = listeners;

define(RadioButton);
