import { define } from "services/sahel.js";

const template = () => /* html */ `
<style>
  :host {
    display: flex;
    align-items: stretch;
    margin: 8px;
  }
</style>
<slot></slot>
`;

const RadioButtons = { as: "radio-buttons" };
RadioButtons.formAssociated = true;
RadioButtons.inheritFrom = HTMLElement;

const onChange = compo => e => {
  compo.element.internals_.setFormValue(e.detail.value);
  compo.element.value = e.detail.value;
};

const listeners = compo => [
  { event: `${compo.element.id}:change`, fn: onChange },
];

RadioButtons.template = template;
RadioButtons.listeners = listeners;

define(RadioButtons);
