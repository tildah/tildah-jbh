import "gen_components/cursor-event.js";
import "mdi-component/mdi-component.js";

const icon = element => element.dataset.icon ? /* html */ `
  <mdi-component path="${element.dataset.icon}" class="icon"></mdi-component>
` : "";

const text = element => element.dataset.text ? /* html */ `
  <span class="text">${element.dataset.text}</span>
` : "";

export default component => {
  const containerId = component.element.parentNode.id;
  const { value } = component.element.dataset;

  return /* html */ `
  <style>
    cursor-event {
      z-index: 2;
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
      height: 100%;
      padding: 8px 16px;
      flex: 1;
      box-sizing: border-box;
      text-transform: uppercase;
      background-color: #c9c9c9;
      cursor: pointer;
      color: rgba(0, 0, 0, .54);
      position: relative;
    }

    :host(:not(:last-child)) {
      border-inline-end: 1px solid rgba(0,0,0, .54);
    }

    cursor-event:hover:after {
      content: "";
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      background-color: rgba(0, 0, 0, .1);
    }

    .icon {
      font-size: 24px;
    }

    .text {
      font-size: 14px;
    }

    .icon + .text {
      font-size: 10px;
    }

    
    :host([active]) cursor-event {
      background-color: #1495ff;
      background-image: linear-gradient(rgba(255, 255, 255, .14), rgba(0,0,0,.12));
    }

    :host([active]) cursor-event .text {
      color: white;
    }
  </style>
  <cursor-event data-click-to="${containerId}:change" data-value="${value}">
    ${icon(component.element)}
    ${text(component.element)}
  </cursor-event>
  `;
};
