import EDFEDForm from "gen_components/edfed-form.js";

class FormGather extends EDFEDForm {

  get data() {
    const ids = this.dataset.forms.split(" ");
    const forms = ids.map(id => document.getElementById(id));
    const data = forms.reduce((a, form) => ({ ...a, ...form.data }), {});
    return data;
  }

}

customElements.define("form-gather", FormGather, { extends: "form" });
