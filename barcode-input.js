import { EDFEDInput } from "../gen_components/edfed-input.js";

class BarcodeInput extends EDFEDInput {

  constructor(...args) {
    super(...args);
    this.handlePress = this.handlePress.bind(this);
    this.handleDown = this.handleDown.bind(this);
    this.addEventListener("keyup", this.handlePress);
    this.addEventListener("keydown", this.handleDown);
  }

  connectedCallback() {
    super.connectedCallback();
    if (this.hasAttribute("autofocus")) this.focus();
  }

  handleDown(e) {
    if (e.key === "Enter") {
      e.preventDefault();
      e.stopPropagation();
    }
  }

  handlePress(e) {
    switch (e.key) {
    case "Process":
      return;
    case "ArrowDown":
    case "Enter":
      this.finishReading(e);
      break;
    default:
      this.readKey(e);
      break;
    }
  }

  readDigit(e) {
    this.value = `${this.value.slice(0, -1)}${e.code.slice(-1)}`;
  }

  readKey(e) {
    if (e.code && e.code.startsWith("Digit"))
      this.readDigit(e);
  }

  finishReading() {
    if (!this.value.length && !this.hasAttribute("allow-empty")) return;
    const { as = "value", ...restDataset } = this.dataset;
    if (this.hasAttribute("event"))
      document.dispatchEvent(new CustomEvent(this.getAttribute("event"), {
        detail: { [as]: this.value, ...restDataset },
      }));
    if (this.hasAttribute("cleanish")) this.value = "";
  }

}

customElements.define("barcode-input", BarcodeInput, { extends: "input" });
