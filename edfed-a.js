import { RouterLink } from "RouterLink.js";
import np from "nested-property";

class EDFEDA extends RouterLink {

  constructor(...args) {
    super(...args);
    this.update = this.update.bind(this);
    const loaderEvent = this.getAttribute("loader-event");
    this.watch();
    if (loaderEvent) document.dispatchEvent(new CustomEvent(loaderEvent));
  }

  update(e) {
    const key = this.getAttribute("key");
    const value = np.get(e.detail, key) || this.getAttribute("empty");
    const template = this.getAttribute("template") || "%s";
    const href = template.replace(/%s/g, value);
    this.setAttribute("href", href);
  }

  watch() {
    document.addEventListener(this.getAttribute("event"), this.update);
  }

  disconnectedCallback() {
    document.removeEventListener(this.getAttribute("event"), this.update);
  }

}

customElements.define("edfed-a", EDFEDA, { extends: "a" });
