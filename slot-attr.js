import np from "nested-property";

class SlotAttr extends HTMLElement {

  constructor(...args) {
    super(...args);
    this.attachShadow({ mode: "open" });
    this.onEvent = this.onEvent.bind(this);
  }

  get template() {
    return /* html*/ `
      <style>
        :host {
          display: none;
        }
      </style>
      <slot></slot>
    `;
  }

  connectedCallback() {
    this.render();
    const slot = this.shadowRoot.querySelector("slot");
    const content = this.getContent(slot);
    this.assignAttr(content);
    this.watch();
  }

  render() {
    this.shadowRoot.innerHTML = "";
    const templateEl = document.createElement("template");
    templateEl.innerHTML = this.template;
    this.shadowRoot.appendChild(templateEl.content.cloneNode(true));
  }

  assignAttr(content) {
    const root = this.getRootNode();
    const target = root.querySelector(this.dataset.target);
    target.setAttribute(this.dataset.as, content);
  }

  getContent(slot) {
    const assigneds = slot.assignedElements();
    const [ el ] = assigneds;
    if (!el) return "";
    return el.tagName === "SLOT" ? this.getContent(el) : el.textContent;
  }

  onEvent({ detail }) {
    const { key = "" } = this.dataset;
    this.assignAttr(np.get(detail, key));
  }

  watch() {
    const { on } = this.dataset;
    if (!on) return;
    document.addEventListener(on, this.onEvent);
  }

  disconnectedCallback() {
    const { on } = this.dataset;
    if (!on) return;
    document.removeEventListener(on, this.onEvent);
  }

}

customElements.define("slot-attr", SlotAttr);
