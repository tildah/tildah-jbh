import template from "./template.js";

const validate = order => {
  const errors = [];
  if (!order.cart.length) {
    errors.push("Should have at least 1 item");
  }
  if (![ "normal", "back" ].includes(order.type)) {
    errors.push("The type is not valid");
  }
  if (!order.total || !order.number) {
    errors.push("Shoudl have total and number");
  }
  return { isValid: errors.length === 0, errors };
};

class OfflinePatch extends HTMLElement {

  constructor(...args) {
    super(...args);
    this.attachShadow({ mode: "open" });
    this.init = this.init.bind(this);
    this.render = this.render.bind(this);
    this.onOffline = this.onOffline.bind(this);
    this.onOnline = this.onOnline.bind(this);
    this.onSyncResponse = this.onSyncResponse.bind(this);
    this.onBulked = this.onBulked.bind(this);
    this.onOrder = this.onOrder.bind(this);
    this.sendOrders = this.sendOrders.bind(this);
    this.sendCount = this.sendCount.bind(this);
  }

  connectedCallback() {
    this.watch();
    this.init();
  }

  init() {
    this.syncMode = "no"; // yes: is syncing, done: has finished sync
    this.render();
    this.onOnline();
  }

  render() {
    this.shadowRoot.innerHTML = "";
    const templateEl = document.createElement("template");
    templateEl.innerHTML = template(this);
    this.shadowRoot.appendChild(templateEl.content.cloneNode(true));
  }

  onOffline() {
    this.syncMode = "no";
    this.render();
  }

  onOnline() {
    if (!this.storedOrders.length || !navigator.onLine) {
      this.syncMode = "no";
      return;
    }
    if (this.syncMode === "yes") return;
    this.syncMode = "yes";
    document.dispatchEvent(new CustomEvent("model:orders:fetch", { detail: {
      action: "bulk",
      method: "POST",
      orders: this.storedOrders,
    } }));
    this.render();
  }

  onSyncResponse({ detail: response }) {
    this.syncMode = response.ok ? "done" : "fail";
    if (response.ok) this.onBulked();
    this.render();
    setTimeout(this.init, 60000);
  }

  onBulked() {
    localStorage.removeItem("offlineOrders");
  }

  get storedOrders() {
    const orders = JSON.parse(localStorage.getItem("offlineOrders") || "[]");
    return orders.map(o => ({ ...o, discount: o.discount || o.total }));
  }

  storeOrder(order) {
    const { storedOrders } = this;
    storedOrders.push(order);
    localStorage.setItem("offlineOrders", JSON.stringify(storedOrders));
  }

  async onOrder({ detail }) {
    if (localStorage.getItem("disguise"))
      return document.dispatchEvent(new CustomEvent("error-popup:show", {
        detail: "Les ventes hors-ligne en déguisement ne sont pas permises",
      }));
    const reader = detail.body.getReader();
    const { value } = await reader.read();
    const decoder = new TextDecoder("utf-8");
    const order = JSON.parse(decoder.decode(value));
    order.createdAt = new Date();
    const validation = validate(order);
    console.log(validation);
    if (!validation.isValid)
      return document.dispatchEvent(new CustomEvent("error-popup:show", {
        detail: "La vente est n'est pas valide",
      }));
    this.storeOrder(order);
    this.render();
    document.dispatchEvent(new CustomEvent("offline-patch:orders:created"));
  }

  sendCount() {
    const evName = "offline-patch:got:orders:count";
    document.dispatchEvent(new CustomEvent(evName, { detail: {
      count: this.storedOrders.length,
    } }));
  }

  sendOrders() {
    const evName = "offline-patch:got:orders";
    document.dispatchEvent(new CustomEvent(evName, {
      detail: this.storedOrders,
    }));
  }

  get watchers() {
    return [
      { event: "offline", listener: this.onOffline, target: window },
      { event: "online", listener: this.onOnline, target: window },
      { event: "orders/bulk:responded", listener: this.onSyncResponse },
      { event: "POST:orders/:503", listener: this.onOrder },
      { event: "offline-patch:get:orders", listener: this.sendOrders },
      { event: "offline-patch:get:orders:count", listener: this.sendCount },
    ];
  }

  watch() {
    this.watchers.forEach(w => {
      (w.target || document).addEventListener(w.event, w.listener);
    });
  }

  disconnectedCallback() {
    this.watchers.forEach(w => {
      (w.target || document).removeEventListener(w.event, w.listener);
    });
  }

}

customElements.define("offline-patch", OfflinePatch);
