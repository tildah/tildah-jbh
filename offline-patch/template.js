import "own_components/styled-text.js";

const syncModeTexts = {
  no: "",
  yes: "Synchronisation...",
  done: "Données synchronisées ✓",
  fail: "Echec de synchronisation ✗",
};

export default component => /* html */ `
  <style>
    .container {
      color: white;
      background-image: linear-gradient(hsl(200, 100%, 62%), hsl(200, 100%, 48%));
      background-color: hsl(200, 100%, 54%);
      font-weight: bold;
      padding: 0 8px;
      height: 24px;
      display: flex;
      align-items: center;
      border-radius: 4px;
    }
    #sync-mode::before {
      content: attr(data-text);
    }
    [data-hide="true"] {
      display: none;
    }
  </style>
  <styled-text data-type="caption" class="container" data-hide="${navigator.onLine && component.syncMode === "no"}">
    <span data-text="${syncModeTexts[component.syncMode]}" id="sync-mode"></span>
    <span data-hide="${navigator.onLine}">
      <span id="status">Vous êtes hors-ligne!</span>
      <span id="data-stored" data-hide="${component.storedOrders.length === 0}">
        ${component.storedOrders.length}
        Ventes sauvegardées.
      </span>
    </span>
  </styled-text>
`;
