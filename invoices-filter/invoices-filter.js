import template from "./template.js";

class InvoicesFilter extends HTMLElement {

  connectedCallback() {
    this.render();
  }

  render() {
    this.innerHTML = "";
    const templateEl = document.createElement("template");
    templateEl.innerHTML = template(this);
    this.appendChild(templateEl.content.cloneNode(true));
  }

}

customElements.define("invoices-filter", InvoicesFilter);
