import "gen_components/edfed-form.js";
import "gen_components/edfed-select.js";
import "own_components/form-field.js";
import "own_components/date-input/date-input.js";
import "own_components/flex-box.js";
import "dispatcher-button/dispatcher-button.js";
import { mdiFilter } from "@mdi/js";

export default component => /* html */ `
  <form is="edfed-form"
    submit-on="invoices-filter:submit"
    submit-to="model:${component.dataset.module}:load"
  >
    <flex-box train>
      <form-field label="Client:">
        <select is="edfed-select" id="invoices-filter-clients-select" name="client">
          <option value="" selected>Tous</option>
        </select>
        <template id="invoices-filter-clients-option">
          <option data-patch="_id:value" data-slot="name"></option>
        </template>
        <map-list id="clients_list"
          data-loader-event="model:clients:load"
          data-loaded-event="model:clients:loaded"
          data-template="invoices-filter-clients-option"
          data-host="invoices-filter-clients-select"
          data-mode="append"
        ></map-list>
      </form-field>
      <form-field label="Du:">
        <input is="date-input" name="from"/>
      </form-field>
      <form-field label="Au:">
        <input is="date-input" name="to"/>
      </form-field>
      <div flex-fill></div>
      <dispatcher-button clk="invoices-filter:submit">
        <own-button
          data-icon="${mdiFilter}"
          data-text=""
        ></own-button>
      </dispatcher-button>
    </flex-box>
`;
