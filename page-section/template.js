export default component => /* html */ `
  <style>
    :host {
      margin: 8px;
      padding: 8px;
      box-sizing: border-box;
      display: ${component.dataset.inline ? "inline-flex" : "flex"};
      flex-direction: column;
      position: relative;
      border-top: 2px solid rgba(0,0,0,.42);
    }
    ::slotted(*) {
      margin: 8px 0;
    }
    :host .title {
      position: absolute;
      top: -15px;
      background-color: rgb(250, 250, 250);
      font-size: 20px;
      color: #444444;
      padding: 0 12px;
    }

    @media only screen and (min-width: 768px) {
      :host {
        padding: 24px;
        margin: 24px 16px;
      }

      ::slotted(*) {
        margin: 8px;
      }
    }
  </style>
  <div class="title">${component.getAttribute("title-text")}</div>
  <slot></slot>
`;
