import template from "./template.js";

class StackList extends HTMLElement {

  constructor(...args) {
    super(...args);
    this.attachShadow({ mode: "open" });
  }

  connectedCallback() {
    this.render();
    this.watch();
  }

  render() {
    this.shadowRoot.innerHTML = "";
    const templateEl = document.createElement("template");
    templateEl.innerHTML = template(this);
    this.shadowRoot.appendChild(templateEl.content.cloneNode(true));
  }

  get watchers() {
    return [
      // Put here you component's watchers in the following format
      // { event: "model:example:action", listener: this.onAction },
    ];
  }

  watch() {
    this.watchers.forEach(watcher => {
      document.addEventListener(watcher.event, watcher.listener);
    });
  }

  disconnectedCallback() {
    this.watchers.forEach(watcher => {
      document.removeEventListener(watcher.event, watcher.listener);
    });
  }

}

customElements.define("stack-list", StackList);
