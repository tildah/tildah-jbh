export default () => /* html */ `
  <style>
    :host {
      padding: 8px 0;
      display: flex;
      flex-direction: column;
      background-color: #fff;
    }

  </style>
  <slot></slot>
`;
