import nestedProperty from "nested-property";

class ArrayMapper extends HTMLElement {

  constructor(...args) {
    super(...args);
    if (!this.id) throw new Error(this.noIDErrorMessage);
    this._list = [];
    this.style.display = "contents";
    this.onLoaded = this.onLoaded.bind(this);
    this.onPush = this.onPush.bind(this);
    this.dispatchItem = this.dispatchItem.bind(this);
    this.resetList = this.resetList.bind(this);
  }

  get loadedEvent() {
    return this.dataset.loadedEvent || `${this.id}:loaded-event`;
  }

  watch() {
    const { loadedEvent, onLoaded, onPush, resetList } = this;
    const { pushFrom, resetOn } = this.dataset;

    document.addEventListener(loadedEvent, onLoaded);
    if (pushFrom) document.addEventListener(pushFrom, onPush);
    if (resetOn) document.addEventListener(resetOn, resetList);
  }

  disconnectedCallback() {
    const { loadedEvent, onLoaded, onPush, reset, dispatchItem } = this;
    const { pushFrom, resetOn } = this.dataset;

    document.removeEventListener(loadedEvent, onLoaded);
    if (pushFrom) document.removeEventListener(pushFrom, onPush);
    if (resetOn) document.addEventListener(resetOn, reset);
    document.addEventListener(`${this.id}:dispatch-item`, dispatchItem);
  }

  resetList() {
    this._list = [];
    this.render();
  }

  onLoaded(e) {
    const { detail } = e;
    const { fromKey = "", mode = "replace" } = this.dataset;
    const loadedList = nestedProperty.get(detail, fromKey) || [];
    const modesNewList = {
      append: () => [ ...this._list, ...loadedList ],
      replace: () => loadedList,
    };
    this._list = modesNewList[mode]();
    this.render();
  }

  onPush(e) {
    const { pushFromKey = "" } = this.dataset;
    const item = nestedProperty.get(e.detail, pushFromKey);
    if (!item) return;
    this._list.push(item);
    this.render();
    this.dispatchPushed(item);
  }

  get loaderEventData() {
    const {
      loadedEvent,
      loaderEvent,
      onLoaded,
      resetOn,
      fromKey,
      itemId,
      pushFrom,
      pushFromKey,
      pushedEvent,
      template,
      host,
      mode,
      ...data
    } = this.dataset;
    return data;
  }

  dispatchLoad() {
    const { loaderEvent } = this.dataset;
    if (!loaderEvent) return;
    document.dispatchEvent(new CustomEvent(loaderEvent, {
      detail: this.loaderEventData,
    }));
  }

  get noIDErrorMessage() {
    return "A map-list component should always have an id";
  }

  get emptySlot() {
    return this.dataset.emptySlot || "";
  }

  get template() {
    try {
      const explicit = this.getRootNode().getElementById(this.dataset.template);
      const internal = this.querySelector("template");
      return explicit || internal;
    } catch (e) {
      return "";
    }
  }

  get host() {
    try {
      return this.getRootNode().getElementById(this.dataset.host) || this;
    } catch (e) {
      return this;
    }
  }

  renderItem(rawItem, $index) {
    const { template } = this;
    const item = { ...rawItem, $index };
    const clone = template.content.cloneNode(true);
    const slots = clone.querySelectorAll("[data-slot]");
    slots.forEach(slot => {
      const content = nestedProperty.get(item, slot.dataset.slot);
      slot.textContent = content || this.emptySlot;
    });
    const patcheds = clone.querySelectorAll("[data-patch]");
    patcheds.forEach(patched => {
      const map = patched.dataset.patch.split(",").map(attr => attr.split(":"));
      map.forEach(([ key, as ]) => {
        const attrName = as || key;
        const value = nestedProperty.get(item, key);
        const tempAttrName = `${attrName}-temp`;
        const patchTemp = patched.getAttribute(tempAttrName) || "%s";
        const attValue = patchTemp.replace(/%s/, value);
        patched.setAttribute(attrName, attValue);
        patched.removeAttribute(tempAttrName);
      });
      patched.removeAttribute("data-patch");
    });
    return clone;
  }

  getInitialHTML() {
    const { mode } = this.dataset;
    const modesContents = {
      append: this.host.innerHTML,
    };
    return modesContents[mode] || "";
  }

  connectedCallback() {
    this.initialHTML = this.getInitialHTML();
    this.watch();
    this.dispatchLoad();
  }

  render() {
    const { host } = this;
    host.innerHTML = this.initialHTML;
    this._list.forEach((item, $index) => {
      const { renderItem } = this;
      host.appendChild(renderItem.bind(this)(item, $index));
    });
  }

  dispatchPushed(item) {
    const { pushedEvent } = this.dataset;
    if (!pushedEvent) return;
    document.dispatchEvent(new CustomEvent(pushedEvent, {
      detail: item,
    }));
  }

  dispatchItem({ detail: { index } }) {
    const item = this._list[index];
    const { dispatchItemTo } = this.dataset;
    document.dispatchEvent(new CustomEvent(dispatchItemTo, { detail: item }));
  }

}

customElements.define("array-mapper", ArrayMapper);
