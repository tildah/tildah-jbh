import { EDFEDInput } from "gen_components/edfed-input.js";
import { datepicker } from "./datepicker.js";
import styleTag from "./style.js";

class DateInput extends EDFEDInput {

  get options() {
    return {
      customDays: [ "Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam" ],
      maxDate: new Date(),
    };
  }

  connectedCallback() {
    this.injectStyle();
    this.picker = datepicker(this, this.options);
    this.setAttribute("autocomplete", "off");
  }

  injectStyle() {
    const existant = document.getElementById("date-input-style");
    if (existant) return;
    const styleContainer = document.createElement("div");
    styleContainer.innerHTML = styleTag;
    document.body.appendChild(styleContainer);
  }

}

customElements.define("date-input", DateInput, { extends: "input" });
