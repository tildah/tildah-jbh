import np from "nested-property";

class CondGuest extends HTMLElement {

  constructor() {
    super();
    const computedStyle = window.getComputedStyle(this);
    this.initialDisplay = computedStyle.getPropertyValue("display");
    if (!this.hasAttribute("start-visible")) this.style.display = "none";
    this.handle = this.handle.bind(this);
    this.watch();
  }

  watch() {
    document.addEventListener(this.dataset.event, this.handle);
  }

  disconnectedCallback() {
    document.removeEventListener(this.dataset.event, this.handle);
  }

  oldWayHandle(detail) {
    const values = this.dataset.condValue.split(",");
    if (values.includes(`${detail[this.dataset.condKey]}`))
      this.shouldShow = true;
  }

  getShouldShow(cond, detail) {
    const [ key, fn, val ] = cond.split(":");
    const argument = np.get(detail, key);
    switch (fn) {
    case "in":
      if (!val.split(",").includes(argument)) return false;
      break;
    case "gt":
      if (argument <= parseInt(val)) return false;
      break;
    case "lt":
      if (argument >= parseInt(val)) return false;
      break;
    case "lngt":
      if (argument.length <= parseInt(val)) return false;
      break;
    case "lnlt":
      if (argument.length >= parseInt(val)) return false;
      break;
    case "false":
      if (argument) return false;
      break;
    case "true":
      if (!argument) return false;
    }
  }

  newWayHandle(detail) {
    this.shouldShow = true;
    const str = this.dataset.condCompact;
    const conds = str.split("&");
    conds.forEach(cond => {
      const shouldShow = this.getShouldShow(cond, detail);
      if (shouldShow !== undefined) this.shouldShow = shouldShow;
    });
  }

  handle({ detail }) {
    this.shouldShow = false;
    if (this.dataset.condKey && this.dataset.condValue)
      this.oldWayHandle(detail);
    if (this.dataset.condCompact)
      this.newWayHandle(detail);
    if (this.shouldShow)
      return this.show();
    this.hide();
  }

  show() {
    this.style.display = this.initialDisplay;
    const { shownEvent } = this.dataset;
    if (!shownEvent) return;
    document.dispatchEvent(new CustomEvent(shownEvent, { detail: {
      ...this.dataset,
    } }));
  }

  hide() {
    this.style.display = "none";
  }

}

customElements.define("cond-guest", CondGuest);
