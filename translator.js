import Polyglot from "services/polyglot.js";

const translator = new Polyglot({
  storageKey: "lang",
  setLangEvent: "translator:set-lang",
  setLangFrom: "value",
});

export default translator;
