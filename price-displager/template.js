import "gen_components/tool-tip/tool-tip.js";
import "gen_components/cursor-event.js";
import { mdiMonitor } from "@mdi/js";

export default () => window.stoqkiPresAv.value ? /* html */ `
  <cursor-event data-click-to="price-displager:window:open">
    <tool-tip data-text="Ouvrir l'afficheur de prix">
      <mdi-component path="${mdiMonitor}" size="24px"></mdi-component>
    </tool-tip>
  </cursor-event>
` : "";
