import template from "./template.js";

class PriceDisplager extends HTMLElement {

  constructor(...args) {
    super(...args);
    this.attachShadow({ mode: "open" });
    this.openPriceDisplay = this.openPriceDisplay.bind(this);
    this.resetList = this.resetList.bind(this);
    this.updateList = this.updateList.bind(this);
  }

  connectedCallback() {
    this.resetList();
    this.present();
    this.watch();
  }

  render() {
    this.shadowRoot.innerHTML = "";
    const templateEl = document.createElement("template");
    templateEl.innerHTML = template(this);
    this.shadowRoot.appendChild(templateEl.content.cloneNode(true));
  }

  async present() {
    if (window.stoqkiPrice) return this.render();
    try {
      window.stoqkiPres = new PresentationRequest("/orders/price");
      window.stoqkiPresAv = await window.stoqkiPres.getAvailability();
      window.stoqkiPresAv.addEventListener("change", () => {
        this.render();
      });
    } catch (e) {
      console.log(`Presentation availability not supported, ${e.name}: ${
        e.message}`);
    }
  }

  async openPriceDisplay() {
    try {
      window.stoqkiPrice = await window.stoqkiPres.start();
    } catch (e) {
      console.log(e);
    }
  }

  resetList() {
    if (window.stoqkiPrice) window.stoqkiPrice.send("reset");
  }

  updateList({ detail }) {
    const total = detail.reduce((t, i) => t + parseFloat(i.total), 0);
    if (window.stoqkiPrice) window.stoqkiPrice.send(total);
  }

  get watchers() {
    return [
      // Put here you component's watchers in the following format
      { event: "price-displager:window:open", listener: this.openPriceDisplay },
      { event: "model:orders:create", listener: this.resetList },
      { event: "orders-create:update-items", listener: this.updateList },
    ];
  }

  watch() {
    this.watchers.forEach(watcher => {
      document.addEventListener(watcher.event, watcher.listener);
    });
  }

  disconnectedCallback() {
    this.watchers.forEach(watcher => {
      document.removeEventListener(watcher.event, watcher.listener);
    });
  }

}

customElements.define("price-displager", PriceDisplager);
