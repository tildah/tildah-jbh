import "gen_components/cursor-event.js";
import "gen_components/event-guest.js";
import "gen_components/edfed-form.js";

class PopUp extends HTMLElement {
  constructor(...args) {
    super(...args);
    this.validate();
    this.attachShadow({ mode: "open" });
    this.onok = this.onok.bind(this);
    this.onShow = this.onShow.bind(this);
  }

  validate() {
    if (this.dataset.onok === this.hideEvent)
      throw new Error(`The value ${this.dataset.onok} in onok causes loop`);
  }

  /* eslint-disable */
  get template() {
    return /* html*/ `
    <style>
      .container {
        background-color: rgba(0, 0, 0, .6);
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100vh;
        z-index: 3;
        display: flex;
        justify-content: center;
        align-items: center;
      }
      .paper {
        background-color: white;
        border-radius: 8px;
        max-width: 500px;
        width: 90%;
        max-height: 500px;
        overflow-y: auto;
      }
      .header {
        font-size: 32px;
        color: rgba(0,0,0, .58);
        padding: 16px;
        margin: 8px 0;
        border-bottom: 1px solid rgba(0,0,0, .12);
        position: sticky;
        top: 0;
      }
      .closer-box {
        display: flex;
        justify-content: flex-end;
        width: 100%;
        position: absolute;
        top: -16px;
        font-size: 24px;
        left: 0;
        padding: 12px;
        box-sizing: border-box;
      }
      .closer {
        cursor: pointer;
      }

      :host([autoclose]) .closer {
        display: none;
      }

      .content {
        padding: 16px;
      }
      .footer {
        padding: 16px;
        margin: 8px 0;
        border-top: 1px solid rgba(0,0,0, .12);
        position: sticky;
        background-color: white;
        bottom: 0;
        display: flex;
        justify-content: flex-end;
      }

      .footer-button {
        margin: 4px;
        height: 36px;
        text-transform: uppercase;
        display: inline-flex;
        justify-content: center;
        align-items: center;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, .54);
        padding: 0 16px;
        border-radius: 4px;
        cursor: pointer;
      }

      .cancel-button {
        border: 1px solid hsl(183, 56%, 50%);
        color: hsl(183, 86%, 33%);
      }

      .ok-button {
        border: 1px solid hsl(183, 56%, 50%);
        background-color: hsl(183, 56%, 50%);
        color: white;
      }

      :host([autoclose]) .ok-button {
        display: none;
      }

      .slotted-form-container {
        display: none;
      }

    </style>
    <event-guest 
      showon="${this.getAttribute("showon")}"
      hideon="${this.hideEvent}"
      >
      <div class="container">
        <div class="paper">
          <div class="header">
            ${this.getAttribute("title-text") || ""}
            <div class="closer-box">
              <cursor-event class="closer" 
                data-click-to="${this.hideEvent}">
                ✖
              </cursor-event>
            </div>
          </div>
          <div class="content">
            <slot></slot>
          </div>
          <div class="footer">
            ${this.cancelTypes.includes(this.type) ? /* html */ `
              <cursor-event 
                class="footer-button cancel-button"
                data-click-to="${this.hideEvent}"
              >
                ${this.cancelText}
              </cursor-event>
            ` : ""}
            <cursor-event
              class="footer-button ok-button"
              data-click-to="${this.dataset.onok || this.hideEvent}"
            >
              ${this.okText}
            </cursor-event>
          </div>
        </div>
      </div>
    </event-guest>
    `;
  }
  /* eslint-enable */

  get type() {
    return this.dataset.type || "confirm";
  }

  get cancelTypes() {
    return [ "confirm" ];
  }

  get cancelText() {
    return this.dataset.cancel_txt || "Cancel";
  }

  get okText() {
    return this.dataset.ok_txt || "OK";
  }

  get hideEvent() {
    return `${this.id}:hide`;
  }

  connectedCallback() {
    this.render();
    this.watch();
  }

  render() {
    this.shadowRoot.innerHTML = "";
    const templateEl = document.createElement("template");
    templateEl.innerHTML = this.template;
    this.shadowRoot.appendChild(templateEl.content.cloneNode(true));
  }

  onok() {
    document.dispatchEvent(new CustomEvent(this.hideEvent));
  }

  onShow() {
    const autoclose = parseInt(this.getAttribute("autoclose"));
    if (!Number.isInteger(autoclose)) return;
    setTimeout(() => {
      document.dispatchEvent(new CustomEvent(this.hideEvent));
    }, autoclose);
  }

  watch() {
    const { onok } = this.dataset;
    if (onok) document.addEventListener(onok, this.onok);
    document.addEventListener(this.getAttribute("showon"), this.onShow);
  }

  disconnectedCallback() {
    const { onok } = this.dataset;
    if (onok) document.removeEventListener(onok, this.onok);
    document.removeEventListener(this.getAttribute("showon"), this.onShow);
  }
}

customElements.define("pop-up", PopUp);
