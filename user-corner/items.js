import {
  mdiCashRemove,
  mdiFileChart,
  mdiAccountDetails,
  mdiCartMinus,
  mdiFileExcel,
  mdiLogoutVariant,
} from "@mdi/js";

export default [
  {
    label: "Les Dépenses",
    icon: mdiCashRemove,
    access: [ "GET EXPENSES", "GET OWN EXPENSES" ],
    href: "/expenses",
    page: "expenses-root",
  },
  {
    label: "Rapport",
    icon: mdiFileChart,
    access: [ "GET REPORTS", "GET OWN REPORTS" ],
    href: "/orders/reports",
    page: "orders-reports",
  },
  {
    label: "Les Clients",
    icon: mdiAccountDetails,
    access: [ "GET CLIENTS" ],
    href: "/clients",
    page: "clients-root",
  },
  {
    label: "Vente En Moins",
    icon: mdiCartMinus,
    access: [ "ADD ORDER" ],
    href: "/orders/create/back",
    page: "orders-back",
  },
  {
    label: "Importer Excel",
    icon: mdiFileExcel,
    access: [ "ADD PRODUCT" ],
    href: "/products/csv/import",
    page: "csv-import",
  },
  {
    label: "Déconnexion",
    icon: mdiLogoutVariant,
    href: "/login/logout",
  },
];
