import "gen_components/edfed-a.js";
import "mdi-component/mdi-component.js";
import "dispatcher-button/dispatcher-button.js";
import LoginManager from "app_modules/login/manager.js";
import items from "./items.js";
import { mdiDotsVertical } from "@mdi/js";

export default component => /* html*/ `
  ${component.style}
    <div class="username">
      <img class="username-logo" src="${component.logoLink}" alt="">
      ${LoginManager.session.name}
    </div>
    <dispatcher-button clk="user-corner:show" class="main-icon">
      <mdi-component size="24px" path="${mdiDotsVertical}"></mdi-component>
    </dispatcher-button>
    <div class="dropdown">
      <div class="title">
        <div class="title-icon">
          <img class="title-logo" src="${component.logoLink}" alt="">
        </div>
        <div>
          <div> ${LoginManager.session.name} </div>
          <div class="store-info">
            ${((LoginManager.session.branch || {}).store || {}).name || ""}
            /
            ${(LoginManager.session.branch || {}).name || ""}
          </div>
        </div>
      </div>
      <div class="content">
        ${items.map(item => component.itemTemplate(item)).join("")}
      </div>
    </div>
  </div>
`;

