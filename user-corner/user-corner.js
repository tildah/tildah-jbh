import LoginManager from "app_modules/login/manager.js";
import { colors, textSizes } from "global-vars.js";
import template from "./template.js";

class UserCorner extends HTMLElement {

  constructor(...args) {
    super(...args);
    this.attachShadow({ mode: "open" });
    this.show = this.show.bind(this);
    this.handleDocClick = this.handleDocClick.bind(this);
    this.render = this.render.bind(this);
  }

  get mainIconStyle() {
    return /* html */ `
        .main-icon {
          width: 32px;
          height: 32px;
          margin: 0 8px;
          border-radius: 2px;
          color: ${colors.main};
          background-color: white;
          display: flex;
          justify-content: center;
          align-items: center;
          font-size: 26px;
          font-weight: bold;
          font-family: sans-serif;
          cursor: pointer;
        }
        .username {
          display: flex;
          align-items: center;
        }
        .username-logo {
          height: 32px;
          padding: 0 8px;
        }
    `;
  }

  get dropdownStyle() {
    return /* html */ `
      .dropdown {
        position: absolute;
        width: 300px;
        right: 0;
        top: 100%;
        background-color: #FFF;
        display: none;
        flex-direction: column;
        align-items: stretch;
        color: #000;
        box-shadow: 0px 4px 12px rgba(0,0,0,.3);
        z-index: 2;
      }

      :host([visible]) .dropdown {
        display: flex;
      }
    `;
  }

  get titleStyle() {
    return /* html */ `
      .title {
        background-color: #eeeeee;
        display: flex;
        flex-direction: row;
        align-items: center;
        padding: 0 16px;
        height: 72px;
        font-size: ${textSizes.subheading};
      }
      .store-info {
        color: ${colors.txtSecondary};
        font-size: ${textSizes.body};
      }
    `;
  }

  get titleIconStyle() {
    return /* html */ `
      .title-icon {
        display: flex;
        flex-direction: row;
        justify-content: flex-start;
        align-items: center;
        width: 56px;
      }
      .title-logo {
        height: 40px;
      }
    `;
  }

  get contentStyle() {
    return /* html */ `
      .content {
        padding: 8px 0;
        display: flex;
        flex-direction: column;
      }
    `;
  }

  get itemStyle() {
    return /* html */ `
      .item {
        display: flex;
        flex-direction: row;
        align-items: center;
        height: 40px;
        padding: 0 16px;
        text-transform: capitalize;
        color: rgba(0, 0, 0, .84);
        text-decoration: none;
      }
      .item:hover {
        cursor: pointer;
        background-color: #f5f5f5;
      }
    `;
  }

  get itemIconStyle() {
    return /* html */ `
      .item-icon {
        width: 56px;
        color: rgba(0, 0, 0, .38);
      }
    `;
  }

  get style() {
    return /* html */ `
      <style>

        :host {
          position: relative;
          margin-right: 16px;
          z-index: 4;
          display: flex;
          align-items: center;
        }

        .username {
          color: ${colors.main};
          display: flex;
          align-items: center;
        }

        :host:hover .dropdown {
          display: flex;
        }
       
       ${this.mainIconStyle}
       ${this.dropdownStyle}
       ${this.titleStyle}
       ${this.titleIconStyle}
       ${this.contentStyle}
       ${this.itemStyle}
       ${this.itemIconStyle}

      </style>
   `;
  }

  itemTemplate(item) {
    const attrIs = item.href.startsWith("http") ? "" : "router-link";
    if (item.access && item.access.every(a => !LoginManager.can(a))) return "";
    return /* html */ `
      <a is="${attrIs}" href="${item.href}" class="item">
        <div class="item-icon">
          <mdi-component size="24px" path="${item.icon}"></mdi-component>
        </div>
        ${item.label}
      </a>
    `;
  }

  get logoLink() {
    if (LoginManager.session.branch && LoginManager.session.branch.store)
      return LoginManager.session.branch.store.logo;
    return "";
  }

  get APIHost() {
    return process.env.API_URL;
  }

  connectedCallback() {
    this.render();
    this.watch();
  }

  render() {
    const templateEl = document.createElement("template");
    templateEl.innerHTML = template(this);
    this.shadowRoot.innerHTML = "";
    this.shadowRoot.appendChild(templateEl.content.cloneNode(true));
  }

  show() {
    this.setAttribute("visible", "true");
  }

  hide() {
    this.removeAttribute("visible");
  }

  handleDocClick(e) {
    const path = e.path || (e.composedPath && e.composedPath());
    const isInside = path.includes(this);
    if (!isInside) {
      this.hide();
    }
  }

  watch() {
    document.addEventListener("user-corner:show", this.show);
    document.addEventListener("click", this.handleDocClick);
    document.addEventListener("auth:user-changed", this.render);
    document.addEventListener("auth:user-reloaded", this.render);
  }

  disconnectedCallback() {
    document.removeEventListener("user-corner:show", this.show);
    document.removeEventListener("click", this.handleDocClick);
    document.removeEventListener("auth:user-changed", this.render);
    document.removeEventListener("auth:user-reloaded", this.render);
  }

}

customElements.define("user-corner", UserCorner);
