const CONTRAST = 127;
const DENSITY = 8;
const HEX_DENSITY = String.fromCharCode(DENSITY);
const IMG_LINESPACING = `\x1b\x33${HEX_DENSITY}`;
const DFT_LINESPACING = "\x1b\x33\x32";

const extractBits = imageData => {
  const res = [];
  for (let i = 0; i < imageData.length; i += 4) {
    const r = imageData[i];
    const g = imageData[i + 1];
    const b = imageData[i + 2];
    const a = imageData[i + 3];
    const avg = Math.floor((r + g + b) / 3);
    const closeToBlack = avg <= CONTRAST;
    const isOpak = a >= CONTRAST;
    const bit = closeToBlack && isOpak ? 1 : 0;
    res.push(bit);
  }
  return res;
};

const chunkByLength = (bits, width, processor = c => c ) => {
  const list = [ ...bits ];
  const empty = new Array(Math.ceil(list.length / width)).fill();
  return empty.map(() => processor(list.splice(0, width)));
};

const transpose = a => Object.keys(a[0]).map(c => a.map(r => r[c]));

const chunkBytes = swap => {
  const toByteString = chunk => chunk.join("").padEnd(DENSITY, "0");
  return swap.map(col => chunkByLength(col, DENSITY, toByteString));
};

const convertHex = columns => {
  const toDecimal = str => parseInt(str, 2).toString(10).toUpperCase();
  const format = str => String.fromCharCode(parseInt(toDecimal(str)));
  return columns.map(col => col.map(byte => format(byte) ));
};

const asPosStrings = (rows, width) => {
  const CMD = `\x1B\x2A\x00${String.fromCharCode(width)}\x00`;
  return rows.map(row => `${CMD}${row.join("")}`);
};

const posImageData = (imageData, width) => {
  const bits = extractBits(imageData);
  const chunks = chunkByLength(bits, width);
  const swap = transpose(chunks);
  const bytes = chunkBytes(swap);
  const hex = convertHex(bytes);
  const rowByRow = transpose(hex);
  const asStrings = asPosStrings(rowByRow, width);
  return `${IMG_LINESPACING}${asStrings.join("\n")}${DFT_LINESPACING}`;
};

export const getLogoData = logo => new Promise(resolve => {
  if (!logo) return resolve("");
  const image = new Image();
  image.crossOrigin = "Anonymous";
  image.onload = () => {
    const canvas = document.createElement("canvas");
    const context = canvas.getContext("2d");
    let height = 70;
    let width = Math.floor((height / image.height) * image.width);
    if (width > 255) width = 255;
    height = Math.floor((width / image.width) * image.height);
    canvas.width = width;
    canvas.height = height;
    context.drawImage(image, 0, 0, width, height);
    const myData = context.getImageData(0, 0, width, height);
    const hex = posImageData(myData.data, myData.width);
    resolve(hex);
  };
  image.src = logo;
});
