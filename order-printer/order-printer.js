import LoginManager from "app_modules/login/manager.js";
import "mdi-component/mdi-component.js";
import "gen_components/cursor-event.js";
import "gen_components/tool-tip/tool-tip.js";
import "own_components/own-link/own-link.js";
import dayjs from "dayjs";
import { getLogoData } from "own_components/order-printer/image-helper.js";
import { TEXT_FORMAT as posTxt, PAPER } from "escpos/commands";
import { mdiRestart, mdiClose, mdiPrinter } from "@mdi/js";

const LINE_LENGTH = 48;

const bold = txt => `${posTxt.TXT_BOLD_ON}${txt}${posTxt.TXT_BOLD_OFF}`;

const fillLine = (part1, part2, filler = " ") => {
  const part1Space = LINE_LENGTH - part2.length;
  return `${part1.padEnd(part1Space, filler)}${part2}`;
};

class OrderPrinter extends HTMLElement {

  constructor(...args) {
    super(...args);
    this.attachShadow({ mode: "open" });
    this.print = this.print.bind(this);
    this.onPrint = this.onPrint.bind(this);
    this.selectPrinter = this.selectPrinter.bind(this);
    this.unsetPrinter = this.unsetPrinter.bind(this);
  }

  get storedPrinter() {
    return JSON.parse(localStorage.getItem("printer") || "{}");
  }

  async selectPrinter() {
    const device = await navigator.usb.requestDevice({ filters: [] });
    const storedKeys = [
      "vendorId",
      "manufacturerName",
      "productName",
      "productId",
      "serialNumber",
    ];
    const stored = storedKeys.reduce((o, k) => ({ ...o, [k]: device[k] }), {});
    localStorage.setItem("printer", JSON.stringify(stored));
    this.render();
  }

  unsetPrinter() {
    localStorage.removeItem("printer");
    this.render();
  }

  async getSelectedPr() {
    const { storedPrinter: sp } = this;
    const devices = await navigator.usb.getDevices();
    const isIt = d => d.vendorId == sp.vendorId && d.productId == sp.productId;
    return devices.find(isIt) || {};
  }

  async printerText() {
    const printer = await this.getSelectedPr();
    const templates = {
      normal: /* html */ `
        ${printer.manufacturerName}-${printer.productName}
        <cursor-event data-click-to="order-printer:select:printer">
          <tool-tip data-text="Changer d'imprimante">
            <mdi-component path="${mdiRestart}"></mdi-component>
          </tool-tip>
        </cursor-event>
        <cursor-event data-click-to="order-printer:unset:printer">
          <tool-tip data-text="Ne pas imprimer">
            <mdi-component path="${mdiClose}"></mdi-component>
          </tool-tip>
        </cursor-event>
      `,
      empty: /* html */ `
      <cursor-event data-click-to="order-printer:select:printer">
        <own-link>Selectionner une imprimante</own-link>
      </cursor-event>
    `,
    };
    return printer.serialNumber ? templates.normal : templates.empty;
  }

  async template() {
    return /* html*/ `
      <style>
        :host {
          display: flex;
          align-items: center;
        }
        .icon {
          margin: 0 8px;
          display: flex;
          align-items: center;
        }
      </style>
      <div class="icon">
        <mdi-component size="24px" path="${mdiPrinter}"></mdi-component>
        : ${await this.printerText()}
      </div>
    `;
  }

  itemTemplate(item) {
    const uPrice = item.price.toFixed(3);
    const total = item.total.toFixed(3);
    const nameRow = `${item.orderQty} x ${item.name}`;
    const priceRow = fillLine(uPrice, ` = ${total} dt`);
    return `${nameRow}\n${priceRow}\n`;
  }

  async orderTemplate(order) {
    const { total, discount, type } = order;
    const esc = "\x1B"; // ESC byte in hex notation
    const fixedTotal = total.toFixed(3);
    const fixedDiscount = discount.toFixed(3);
    const discountRow = fillLine("PRIX REMISE", `${fixedDiscount} dt`);
    const madeDiscount = fixedTotal !== fixedDiscount;
    const displayDiscount = madeDiscount ? `${discountRow}\n` : "";
    const dashLine = `${fillLine("", "", "-")}\n`;
    const img = await getLogoData(LoginManager.session.branch.store.logo);
    const templateParts = [
      `${posTxt.TXT_ALIGN_CT}`,
      `${img}\n`,
      `${posTxt.TXT_ALIGN_LT}`,
      `${esc}!\x38${LoginManager.session.branch.store.name}\n${esc}!\x00`,
      `${order.branch.name}\n`,
      type === "back" ? "Ticket Retour!\n" : "",
      dashLine,
      `\n${order.cart.map(this.itemTemplate).join("")}\n`,
      `${bold(fillLine("TOTAL", `${fixedTotal} dt`))}\n`,
      displayDiscount,
      dashLine,
      `Numero Achat: ${order.number}\n`,
      `${dayjs(order.date).format("DD/MM/YYYY HH:mm:ss")}`,
      `\n\n${LoginManager.session.branch.store.billfooter || ""}`,
      this.hasAttribute("data-copy") ? "\n\nCeci est un duplicata!" : "",
      `\n\n\n\n\n\n${PAPER.PAPER_FULL_CUT}`,
    ];
    return templateParts.join("");
    // I used an array of strings instead of a template string to avoid indents
    // and unwanted line breaks in printing
  }

  onPrint({ detail }) {
    // This prop prevents the component from printing everytime the order is
    // loaded, since other component need to load the order
    this.openPrinting = true;
    document.dispatchEvent(new CustomEvent("model:orders:load-by-id", {
      detail: detail._id,
    }));
  }

  selectEndpoint(direction, device) {
    const endpoint = device.configuration
      .interfaces[0]
      .alternate
      .endpoints.find(ep => ep.direction == direction);

    if (endpoint == null)
      throw new Error(`Endpoint ${direction} not found in device interface.`);
    return endpoint;
  }

  async print(e) {
    if (!this.openPrinting) return;
    this.openPrinting = false;
    const device = await this.getSelectedPr();
    await device.open();
    await device.selectConfiguration(1);
    const { interfaceNumber } = device.configuration.interfaces[0];
    await device.claimInterface(interfaceNumber);
    const str = await this.orderTemplate(e.detail);
    const bytes = str.split("").map(char => char.charCodeAt(0));
    const data = new Uint8Array(bytes);
    const endpoint = this.selectEndpoint("out", device);
    await device.transferOut(endpoint.endpointNumber, data);
    await device.close();
    document.dispatchEvent(new CustomEvent("order-printer:printed"));
  }

  async connectedCallback() {
    await this.render();
    this.watch();
  }

  async render() {
    this.shadowRoot.innerHTML = "";
    const templateEl = document.createElement("template");
    templateEl.innerHTML = await this.template();
    this.shadowRoot.appendChild(templateEl.content.cloneNode(true));
  }

  get watchers() {
    return [
      { event: "order-printer:print", listener: this.onPrint },
      { event: "order-printer:select:printer", listener: this.selectPrinter },
      { event: "order-printer:unset:printer", listener: this.unsetPrinter },
      { event: "model:orders:loaded-by-id", listener: this.print },
    ];
  }

  watch() {
    this.watchers.forEach(watcher => {
      document.addEventListener(watcher.event, watcher.listener);
    });
  }

  disconnectedCallback() {
    this.watchers.forEach(watcher => {
      document.addEventListener(watcher.event, watcher.listener);
    });
  }
}

customElements.define("order-printer", OrderPrinter);
