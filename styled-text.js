import { textSizes, colors } from "global-vars.js";

class StyledText extends HTMLElement {

  constructor(...args) {
    super(...args);
    this.attachShadow({ mode: "open" });
  }

  get colors() {
    return {
      primary: colors.txtPrimary,
      secondary: colors.txtSecondary,
      hint: colors.txtHint,
    };
  }

  get template() {
    return /* html*/ `
      <style>
        .content {
          font-size: ${textSizes[this.dataset.type || "body"]};
          color: ${this.colors[this.dataset.color] || "inherit"};
        }
        :host([block]) {
          display: block;
        }
        :host([secondary]) {
          color: ${this.colors.secondary};
        }
      </style>
      <span class="content">
        <slot></slot>
      </span>
    `;
  }

  connectedCallback() {
    this.render();
  }

  render() {
    this.shadowRoot.innerHTML = "";
    const templateEl = document.createElement("template");
    templateEl.innerHTML = this.template;
    this.shadowRoot.appendChild(templateEl.content.cloneNode(true));
  }

}

customElements.define("styled-text", StyledText);
