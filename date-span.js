import dayjs from "dayjs";

class DateSpan extends HTMLElement {

  constructor() {
    super();
    this.onEvent = this.onEvent.bind(this);
    this.render();
    this.watch();
  }

  render() {
    this.date = this.getAttribute("date");
    if (!this.date) return;
    const format = this.getAttribute("format");
    const content = dayjs(this.date).format(format);
    this.innerHTML = content;
  }

  static get observedAttributes() {
    return [ "date" ];
  }

  async attributeChangedCallback() {
    this.render();
  }

  onEvent(e) {
    this.setAttribute("date", e.detail[this.dataset.key]);
  }

  watch() {
    if (this.dataset.event)
      document.addEventListener(this.dataset.event, this.onEvent);
  }

  disconnectedCallback() {
    if (this.dataset.event)
      document.removeEventListener(this.dataset.event, this.onEvent);
  }

}

customElements.define("date-span", DateSpan);
