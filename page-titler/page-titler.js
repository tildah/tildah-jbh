import LoginManager from "app_modules/login/manager.js";

class PageTitler extends HTMLElement {

  constructor(...args) {
    super(...args);
    this.attachShadow({ mode: "open" });
    this.onPageLoaded = this.onPageLoaded.bind(this);
  }

  connectedCallback() {
    this.watch();
  }

  get watchers() {
    return [
      { event: "router:page-loaded", listener: this.onPageLoaded },
    ];
  }

  get baseTitle() {
    const { branch = {} } = LoginManager.session;
    const { store = {} } = branch;
    return store.name || "Stoqki";
  }

  onPageLoaded({ detail }) {
    const title = `${this.baseTitle} ${detail.component.title || ""}`;
    document.title = title;
  }

  watch() {
    this.watchers.forEach(watcher => {
      document.addEventListener(watcher.event, watcher.listener);
    });
  }

  disconnectedCallback() {
    this.watchers.forEach(watcher => {
      document.removeEventListener(watcher.event, watcher.listener);
    });
  }

}

customElements.define("page-titler", PageTitler);
