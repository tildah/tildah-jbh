import template from "./template.js";

class CartList extends HTMLElement {

  constructor(...args) {
    super(...args);
    this.attachShadow({ mode: "open" });
    this.toggle = this.toggle.bind(this);
    this.submit = this.submit.bind(this);
    this.push = this.push.bind(this);
    this.updateBranch = this.updateBranch.bind(this);
  }

  push(e) {
    const product = e.detail;
    product.orderQty = parseInt(product.orderQty);
    const existentIndex = this.list.findIndex(p => p._id === product._id);
    const deleteCount = existentIndex > -1 ? 1 : 0;
    const args = [
      existentIndex,
      deleteCount,
      ...(product.orderQty ? [ product ] : []),
    ];
    this.list.splice(...args);
    this.render();
  }

  updateBranch({ detail }) {
    this.branchId = detail.value;
  }

  toggle() {
    this.show = !this.show;
    this.render();
  }

  get submitDataset() {
    const { submitTo, term, branchOn, ...data } = this.dataset;
    return data;
  }

  submit() {
    if (!this.list.length) return;
    const { branchId } = this;
    document.dispatchEvent(new CustomEvent(this.submitEvent, {
      detail: {
        cart: this.list,
        ...this.submitDataset,
        ...(branchId ? { branchId } : {}),
      },
    }));
  }

  get term() {
    return this.dataset.term || "demande";
  }

  get tabIcon() {
    return this.show ? "chevron-down" : "chevron-up";
  }

  get totalQty() {
    return this.list.reduce((t, item) => t + parseInt(item.orderQty), 0);
  }

  get totalPrice() {
    return this.list.reduce(
      (t, i) => t + (parseFloat(i.price) * parseInt(i.orderQty)),
      0,
    );
  }

  get submitEvent() {
    return this.dataset.submitTo || "model:transfers:create";
  }

  connectedCallback() {
    this.show = false;
    this.list = [];
    this.render();
    this.watch();
  }

  watch() {
    document.addEventListener("cart-list:toggle", this.toggle);
    document.addEventListener("cart-list:submit", this.submit);
    document.addEventListener(this.getAttribute("push-event"), this.push);
    document.addEventListener(this.dataset.branchOn, this.updateBranch);
  }

  disconnectedCallback() {
    document.removeEventListener("cart-list:toggle", this.toggle);
    document.removeEventListener("cart-list:submit", this.submit);
    document.removeEventListener(this.getAttribute("push-event"), this.push);
  }

  render() {
    this.shadowRoot.innerHTML = "";
    const templateEl = document.createElement("template");
    templateEl.innerHTML = template(this);
    this.shadowRoot.appendChild(templateEl.content.cloneNode(true));
  }

}

customElements.define("cart-list", CartList);
