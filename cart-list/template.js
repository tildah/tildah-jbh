import "mdi-component/mdi-component.js";
import "dispatcher-button/dispatcher-button.js";

export default component => /* html */ `
    <style>
      :host {
        position: fixed;
        bottom: 0;
        left: 0;
        right: 0;
        display: flex;
        justify-content: center;
      }

      .tab {
        background-color: #089bc7;
        color: white;
        position: absolute;
        top: -40px;
        height: 40px;
        width: 40px;
        display: flex;
        justify-content: center;
        align-items: center;
        box-shadow: 2px -2px 6px rgba(0, 0, 0, .12);
        cursor: pointer;
      }

      .container {
        border: 1px solid rgba(0, 0, 0, .12);
        box-shadow: 2px -2px 6px rgba(0, 0, 0, .12);
        background-color: white;
      }

      .header {
        background-color: #089bc7;
        color: white;
        height: 68px;
        display: flex;
        align-items: center;
        justify-content: center;
        padding: 8px;
      }

      .body {
        display: ${component.show ? "block" : "none"};
      }

      .item {
        display: grid; 
        padding: 8px;
        border-bottom: 1px solid rgba(0, 0, 0, .12);
      }

      .item-name {
        font-size: 20px;
        grid-column-start: 1;
        grid-row-start: 1;
      }

      .item-price {
        color: rgba(0, 0, 0, .54);  
        grid-column-start: 1;
        grid-row-start: 2;
      }

      .item-qty {
        grid-column-start: 2;
        grid-row-start: 1;
      }

      .item-total {
        color: rgba(0, 0, 0, .54);  
        grid-column-start: 2;
        grid-row-start: 2;
      }

      .footer {
        padding: 8px;
        display: ${component.show ? "flex" : "none"};
        flex-direction: row;
        align-items: center;
      }

      .total {
        flex: 1;
      }

      .footer button {
        font-size: 16px;
        text-transform: uppercase;
        height: 32px;
      }
    </style>
    <dispatcher-button clk="cart-list:toggle" class="tab">
      <mdi-component size="24px" name="${component.tabIcon}"></mdi-component>
    </dispatcher-button>
    <div class="container">
      <div class="header">
        Votre ${component.term} contient  
        <strong> ${component.list.length} </strong> articles 
        (<strong>${component.totalQty}</strong> pieces)
      </div>
      <div class="body">
        ${component.list.map(item => /* html */ `
          <div class="item">
            <div class="item-name">
              ${item.name}
            </div>
            <div class="item-price">Prix U: ${item.price}dt</div>
            <div class="item-qty">${item.orderQty} pieces</div>
            <div class="item-total">= ${(item.orderQty * item.price).toFixed(2)} dt</div>
          </div>
        `).join("")}
      </div>
      <div class="footer">
        <div class="total">
          TOTAL: <br/>
          ${component.totalPrice} dt
        </div>
        <dispatcher-button clk="cart-list:submit">
          <button>Confirmer</button>
        </dispatcher-button>
      </div>
    </div>
`;
