export default () => /* html */ `
<style>
    :host {
      border-bottom: 1px solid rgba(0,0,0,.12);
      padding: 8px;
      display: flex;
      align-items: center;
      justify-content: center;
      height: 40px;
    }

    :host([grid-only]) {
      display: none;
    }

    @media only screen and (max-width: 768px) {
      :host(:not([grid-role])) {
        display: none;
      }

      :host([grid-only]) {
        display: block;
      }
      
      :host([grid-role="illustration"]) {
        grid-area: ill;
        display: flex;
        align-items: center;
        justify-content: center;
      }

      :host([grid-role="title"]) {
        grid-area: tlt;
        justify-content: flex-start;
      }

      :host([grid-role="subtitle"]) {
        grid-area: sbt;
        color: rgba(0, 0, 0, .54);
        justify-content: flex-start;
      }

      :host([grid-role="note"]) {
        grid-area: nte;
        display: flex;
        align-items: center;
        justify-content: center;
      }

      :host {
        border-bottom: none;
        position: relative;
        height: auto;
      }
    }
</style>
<slot></slot>
`;
