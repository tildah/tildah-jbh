class EDFEDImg extends HTMLImageElement {

  constructor(...args) {
    super(...args);
    const loaderEvent = this.getAttribute("loader-event");
    this.watch();
    if (this.hasAttribute("hide")) this.style.display = "none";
    if (loaderEvent) document.dispatchEvent(new CustomEvent(loaderEvent));
  }

  watch() {
    document.addEventListener(this.getAttribute("event"), e => {
      const key = this.getAttribute("key");
      this.src = e.detail[key];
    });
  }

}

customElements.define("edfed-img", EDFEDImg, { extends: "img" });
