class RouterLink extends HTMLAnchorElement {
  constructor() {
    super();
    this.addEventListener("click", e => {
      const href = this.getAttribute("href");
      const route = href.replace(/%s/, this.dataset.sub);
      window.history.pushState({}, route, route);
      e.preventDefault();
    });
  }
}

customElements.define("router-link", RouterLink, { extends: "a" });

export { RouterLink };
