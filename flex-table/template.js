export default component => {
  const columns = component.dataset.columns.split(",");
  return /* html*/ `
  <style>
    :host {
      position: relative;
      background-color: #fff;
      color: rgba(0, 0, 0, .84);
      display: grid;
      grid-template-columns: repeat(${columns.length}, auto);
      box-shadow: 0px 2px 4px rgba(0, 0, 0, .12);
      border: 1px solid rgba(0, 0, 0, .12);
      border-radius: 16px;
      margin: 4px;
      width: 100%;
      box-sizing: border-box;
    }

    :host([data-height]) {
      overflow-y: scroll;
      max-height: ${component.dataset.height};
    }

    .list-header-cell {
      position: sticky;
      top: 0;
    }

    .list-header {
      font-size: 1.14rem;
      display: contents;
      padding: 0 8px;
      cursor: pointer;
    }

    .list-header-cell {
      color: rgba(0, 0, 0, .34);
      text-align: center;
      padding: 8px;
      text-transform: capitalize;
      font-size: 14px;
      border-bottom: 1px solid rgba(0, 0, 0, .12);
    }

    .list-header-cell:first-child {
      border-top-left-radius: 16px;
    }

    .list-header-cell:last-child {
      border-top-right-radius: 16px;
    }

    @media only screen and (max-width: 768px) {
      :host {
        grid-template-columns:  auto;
        margin: 0;
        border-radius: 0;
      }

      .list-header {
        display: none;
      }
    }
  </style>
  <div class="list-header" >
    ${columns.map(column => /* html */ `
      <div class="list-header-cell">${column}</div>
    `).join("")}
  </div>
  <slot></slot>
`;
};
