import template from "./template.js";

class FlexTable extends HTMLElement {

  constructor(...args) {
    super(...args);
    this.shadow = this.attachShadow({ mode: "open" });
  }

  connectedCallback() {
    this.render();
  }

  render() {
    this.shadow.innerHTML = "";
    const templateEl = document.createElement("template");
    templateEl.innerHTML = template(this);
    this.shadow.appendChild(templateEl.content.cloneNode(true));
  }

}

customElements.define("flex-table", FlexTable);
