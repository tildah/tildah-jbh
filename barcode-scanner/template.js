import "mdi-component/mdi-component.js";
import "gen_components/cursor-event.js";
import {
  mdiCameraSwitch,
  mdiEmoticonSad,
  mdiClose,
} from "@mdi/js";

const cameraName = component => /* html */ `
  <span>${component.selectedDevice.label}</span>
`;

const switcher = component => /* html */ `
  <cursor-event data-click-to="${component.id}:camera:switch">
    <mdi-component path="${mdiCameraSwitch}" size="28" style="margin: 8px 0"></mdi-component>
  </cursor-event>
`;

const video = /* html */ `
  <div class="camera-container">
    <video id="camera-canvas"></video>
    <div class="barcode-line shake-vertical"></div>
  </div>
`;

const noDevice = /* html */ `
  <div class="no-device">
    <mdi-component path="${mdiEmoticonSad}" size="40"></mdi-component>
    <div>Nous ne trouvons pas de caméra!</div>
  </div>
`;

export default component => /* html */ `
  <style>

    :host {
      width: 100%;
      height: 100%;
      display: flex;
      align-items: stretch;
      flex-direction: column;
      border: 2px solid white;
      background-color: #333; 
      position: relative;
    }

    .camera-container {
      flex: 1;
      position: relative;
      min-height: 100px;
    }

    #camera-canvas {
      position: absolute;
      height: 100%;
      width: 100%;
      top: 0;
      left: 0;
    }

    /**
     * ----------------------------------------
     * animation shake-vertical
     * ----------------------------------------
     */
    .shake-vertical {
      -webkit-animation: shake-vertical 2s linear infinite forwards;
              animation: shake-vertical 2s linear infinite forwards;
    }
    @-webkit-keyframes shake-vertical {
      0%,
      100% {
        -webkit-transform: translateY(0);
                transform: translateY(0);
      }
      25%{
        -webkit-transform: translateY(-20vh);
                transform: translateY(-20vh);
      }
      50%{
        -webkit-transform: translateY(0);
                transform: translateY(0);
      }
      75%{
        -webkit-transform: translateY(20vh);
                transform: translateY(20vh);
      }
    }

    .barcode-line{
      top: 50%; /* vertical center */
      left: 20%; /* 20% 60% 20% */
      width: 60%;
      border-bottom: 5px solid red;
      position: absolute;
    }

    .no-device {
      color: white;
      width: 100%;
      flex: 1;
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
      padding: 24px;
      box-sizing: border-box;
    }

    .header {
      background-color: #333; 
      display: flex;
      align-items: center;
      color: white;
      padding: 8px;
    }

    .header .flex {
      flex: 1;
    }
  </style>
  <div class="header">
    ${component.devices.length ? cameraName(component) : ""}
    ${component.devices.length > 1 ? switcher(component) : ""}
    <div class="flex"></div>
    ${component.hasAttribute("hide-exit-button") ? "" : `
      <cursor-event class="closer" data-click-to="${component.id}:camera:hide">
        <mdi-component path="${mdiClose}" size="28"></mdi-component>
      </cursor-event>
    `}
  </div>
  ${component.devices.length ? video : noDevice}
`;
