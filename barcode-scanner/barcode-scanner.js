import { BrowserBarcodeReader } from "@zxing/library";
import template from "./template.js";

class BarcodeScanner extends HTMLElement {

  constructor(...args) {
    super(...args);
    this.attachShadow({ mode: "open" });
    this.id = this.id || "barcode-scanner";
    this.onCameraResult = this.onCameraResult.bind(this);
    this.onShow = this.onShow.bind(this);
    this.onHide = this.onHide.bind(this);
    this.onSwitch = this.onSwitch.bind(this);
  }

  async connectedCallback() {
    this.type = "input";
    this.audio = new Audio("/audio/beep.mp3");
    await this.initCamera();
    this.render();
    this.watch();

    if (this.hasAttribute("scan-on-start")) this.onShow();
  }

  render() {
    this.shadowRoot.innerHTML = "";
    const templateEl = document.createElement("template");
    templateEl.innerHTML = template(this);
    this.shadowRoot.appendChild(templateEl.content.cloneNode(true));
  }

  async initCamera() {
    this.codeReader = new BrowserBarcodeReader();
    this.devices = await this.codeReader.listVideoInputDevices();
  }

  get selectedDevice() {
    const deviceId = localStorage.getItem("cameraId");
    const device = this.devices.find(d => d.deviceId === deviceId);
    return device || this.devices[0];
  }

  get selectedIndex() {
    const deviceId = localStorage.getItem("cameraId");
    const index = this.devices.findIndex(d => d.deviceId === deviceId);
    return index > 0 ? index : 0;
  }

  async onShow() {
    const canvas = this.shadowRoot.querySelector("#camera-canvas");
    const id = this.selectedDevice.deviceId;
    const result = await this.codeReader.decodeOnceFromVideoDevice(id, canvas);
    this.audio.play();
    this.onCameraResult(result);
    const { mode = "hide" } = this.dataset;
    if (mode === "hide") return this.hideCamera();
    this.onHide();
    await this.onShow();
  }

  hideCamera() {
    document.dispatchEvent(new CustomEvent(`${this.id}:camera:hide`));
  }

  onCameraResult(result) {
    const sendAs = this.dataset.as || "value";
    const detail = sendAs === "$ROOT" ? result.text : {
      [sendAs]: result.text,
      ...this.dataset,
    };
    console.log(detail);
    document.dispatchEvent(new CustomEvent(this.dataset.event, { detail }));
  }

  onHide() {
    this.codeReader.reset();
  }

  async onSwitch() {
    const nextIndex = (this.selectedIndex + 1) % this.devices.length;
    localStorage.setItem("cameraId", this.devices[nextIndex].deviceId);
    this.onHide();
    this.render();
    await this.onShow();
  }


  watch() {
    document.addEventListener(`${this.id}:camera:show`, this.onShow);
    document.addEventListener(`${this.id}:camera:hide`, this.onHide);
    document.addEventListener(`${this.id}:camera:switch`, this.onSwitch);
  }

  disconnectedCallback() {
    this.hideCamera();
    this.onHide();
    document.removeEventListener(`${this.id}:camera:show`, this.onShow);
    document.removeEventListener(`${this.id}:camera:hide`, this.onHide);
    document.removeEventListener(`${this.id}:camera:switch`, this.onSwitch);
  }

}

customElements.define("barcode-scanner", BarcodeScanner);
