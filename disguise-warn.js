import "gen_components/edfed-a.js";

class DisguiseWarn extends HTMLElement {

  constructor(...args) {
    super(...args);
    this.attachShadow({ mode: "open" });
    this.render = this.render.bind(this);
  }

  get style() {
    return /* html */ `
      <style>
        :host {
          background-color: #ff7824;
          width: 100%;
          padding: 4px 8px;
          box-sizing: border-box;
          display: flex;
          align-items: center;
        }
        
        .text {
          flex: 1;
        }

        .close {
          color: white;
        }
      </style>
   `;
  }

  get template() {
    return /* html*/ `
      ${this.style}
      <div class="text">
        Vous naviguer le site en déguisement 
      </div>
      <a
        class="close"
        is="edfed-a" 
        event="auth:user-loaded"
        key="host"
        empty="${process.env.API_URL}"
        loader-event="auth:load-user"
        template="/login/reguise">
        Cliquer ici pour quitter
      </a>
    `;
  }

  connectedCallback() {
    this.watch();
  }

  render() {
    this.shadowRoot.innerHTML = "";
    const disguise = window.localStorage.getItem("disguise");
    if (!disguise) return;
    const templateEl = document.createElement("template");
    templateEl.innerHTML = this.template;
    this.shadowRoot.appendChild(templateEl.content.cloneNode(true));
  }

  watch() {
    document.addEventListener("router:page-loaded", this.render);
  }

  disconnectedCallback() {
    document.removeEventListener("router:page-loaded", this.render);
  }

}

customElements.define("disguise-warn", DisguiseWarn);
