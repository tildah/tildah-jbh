import "gen_components/cursor-event.js";
import "gen_components/event-guest.js";
import "gen_components/tool-tip/tool-tip.js";
import "mdi-component/mdi-component.js";
import {
  mdiCamera,
  mdiCameraOff,
} from "@mdi/js";

export default component => /* html */ `
  <event-guest start-visible
    showon="${component.dataset.scannerId}:camera:hide"
    hideon="${component.dataset.scannerId}:camera:show">
    <cursor-event data-click-to="${component.dataset.scannerId}:camera:show">
      <tool-tip data-text="Scanner par camera">
        <mdi-component path="${mdiCamera}" size="24"></mdi-component>
      </tool-tip>
    </cursor-event>
  </event-guest>
  <event-guest showon="${component.dataset.scannerId}:camera:show" hideon="${component.dataset.scannerId}:camera:hide">
    <cursor-event data-click-to="${component.dataset.scannerId}:camera:hide" >
      <tool-tip data-text="Cacher camera">
        <mdi-component mdi="${mdiCameraOff}"></mdi-component>
      </tool-tip>
    </cursor-event>
  </event-guest>
`;
