import LoginManager from "app_modules/login/manager.js";

class AccessGuest extends HTMLElement {

  constructor(...args) {
    super(...args);
    this.attachShadow({ mode: "open" });
  }

  get accessed() {
    return LoginManager.can(this.dataset.access);
  }

  get accessedTemplate() {
    return /* html*/ `
      <slot></slot>
    `;
  }

  get refusedTemplate() {
    return /* html*/ `
      <slot name="on-refuse"></slot>
    `;
  }

  get template() {
    return this.accessed ? this.accessedTemplate : this.refusedTemplate;
  }

  connectedCallback() {
    this.render();
  }

  render() {
    this.shadowRoot.innerHTML = "";
    const templateEl = document.createElement("template");
    templateEl.innerHTML = this.template;
    this.shadowRoot.appendChild(templateEl.content.cloneNode(true));
  }

}

customElements.define("access-guest", AccessGuest);
