import template from "./template.js";

class SideBar extends HTMLElement {

  constructor(...args) {
    super(...args);
    this.attachShadow({ mode: "open" });
    this.render = this.render.bind(this);
    this.toggle = this.toggle.bind(this);
    this.hide = this.hide.bind(this);
    this.handleDocClick = this.handleDocClick.bind(this);
  }

  connectedCallback() {
    this.render();
    this.watch();
  }

  toggle() {
    this.classList.toggle("show");
  }

  hide() {
    this.classList.remove("show");
  }

  handleDocClick(e) {
    const path = e.path || (e.composedPath && e.composedPath());
    if (path.some(el => el.hasAttribute && el.hasAttribute("side-bar-toggler")))
      return this.toggle();
    const isInside = path.includes(this);
    if (!isInside) {
      this.hide();
    }
  }

  render() {
    this.shadowRoot.innerHTML = "";
    const templateEl = document.createElement("template");
    templateEl.innerHTML = template(this);
    this.shadowRoot.appendChild(templateEl.content.cloneNode(true));
  }

  watch() {
    document.addEventListener("side-bar:toggle", this.toggle);
    document.addEventListener("router:page-loaded", this.hide);
    document.addEventListener("click", this.handleDocClick);
    document.addEventListener("auth:user-changed", this.render);
    document.addEventListener("auth:user-reloaded", this.render);
  }

  disconnectedCallback() {
    document.removeEventListener("side-bar:toggle", this.toggle);
    document.removeEventListener("router:page-loaded", this.hide);
    document.removeEventListener("click", this.handleDocClick);
    document.removeEventListener("auth:user-changed", this.render);
    document.removeEventListener("auth:user-reloaded", this.render);
  }

}

customElements.define("side-bar", SideBar);
