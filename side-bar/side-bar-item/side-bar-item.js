import { define } from "services/sahel.js";
import template from "./template.js";

const SideBarItem = { as: "side-bar-item" };
SideBarItem.inheritFrom = HTMLElement;

const onReload = compo => e => {
  const activePage = e.detail.component.is;
  const { page } = compo.element.dataset;
  if (activePage === page) compo.element.setAttribute("active", "true");
  else compo.element.removeAttribute("active");
};

const listeners = () => [
  { event: "router:page-loaded", fn: onReload },
];

SideBarItem.template = template;
SideBarItem.listeners = listeners;

define(SideBarItem);
