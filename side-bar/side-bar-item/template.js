export default compo => {
  const { href } = compo.element.dataset;
  const attrIs = href.startsWith("http") ? "" : "router-link";
  return /* html */ `
    <style>
      .link {
        display: flex;
        flex-direction: row;
        align-items: center;
        height: 40px;
        text-transform: capitalize;
        color: white;
        text-decoration: none;
        flex-wrap: nowrap;
        flex-shrink: 0;
        overflow: hidden;
        -webkit-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
      }
      :host([active]) .link { 
        background-color: #1495ff;
      }
      :host(:not([active])) .link:hover {
        cursor: pointer;
        background-color: rgba(255, 255, 255, 0.34);
      }
      .icon {
        width: 56px;
        min-width: 56px;
        color: white;
        text-align: center; 
      }
      .label {
        width: 148px;
        padding: 0 16px 0 8px;
        overflow: hidden;
        text-overflow: clip;
        white-space: nowrap;
        -webkit-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
      }

    </style>
    <a is="${attrIs}" href="${href}" class="link">
      <div class="icon">
        <mdi-component 
          size="24px"
          path="${compo.element.dataset.icon}"
          ${compo.element.dataset.iconAttributes}
        ></mdi-component>
      </div>
      <div class="label">
        ${compo.element.dataset.label}
      </div>
    </a>
  `;
};
