import "gen_components/edfed-a.js";
import "mdi-component/mdi-component.js";
import "dispatcher-button/dispatcher-button.js";
import "./side-bar-item/side-bar-item.js";
import LoginManager from "app_modules/login/manager.js";

import items from "./items.js";

const getSubscription = () => {
  const { branch = { store: {} } } = LoginManager.session;
  const { subscription = "" } = branch.store;
  return subscription;
};

const headerStyle = /* html */ `
  .header {
    align-self: stretch;
    display: flex;
    text-decoration: none !important; 
  }
  .header-logo {
    background-color: #1495ff; 
    width: 56px;
    height: 40px;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  .header-logo img {
    height: 44px;
  }

  .header-name {
    background-color: white;
    color: #1495ff;
    flex: 1;
    display: flex;
    align-items: center;
    width: 0px;
    overflow: hidden;
    text-overflow: clip;
    white-space: nowrap;
    padding: 0 16px 0 8px;
    -webkit-transition: all 0.3s ease-in-out;
    transition: all 0.3s ease-in-out;
  }

  @media only screen and (min-width: 768px) {
    .header-name {
      padding: 0;
    }

    .content:hover .header-name {
      padding: 0 16px 0 8px;
    }
  }

`;

const contentStyle = /* html */ `
  .content {
    display: flex;
    flex-direction: column;
    -webkit-transition: all 0.3s ease-in-out;
    transition: all 0.3s ease-in-out;
  }
  .list {
    padding: 8px 0;
    display: flex;
    flex-direction: column;
  }

  @media only screen and (min-width: 768px) {
    .content {
      width: 56px;
    }

    .content:hover {
      width: 228px;
    }
  }
`;

function itemTemplate(component, item) {
  if (item.access && !LoginManager.can(item.access)) return "";
  return /* html */ `
    <side-bar-item
      data-href="${item.href}"
      data-icon="${item.icon}"
      data-label="${item.label}"
      data-page="${item.page}"
      data-icon-attributes="${(item.iconAttributes || []).join(" ")}"
    ></side-bar-item>
  `;
}

export default component => /* html */ `
  <style>

    :host {
      z-index: 3;
      position: fixed;
      left: -230px;
      top: 0;
      height: 100%;
      background-color: #292929;
      display: flex;
      flex-direction: column;
      align-items: flex-start;
      box-shadow: 0px 2px 4px rgba(0, 0, 0, .54);
      transition: all 0.3s ease-in-out;
      -webkit-transition: all 0.3s ease-in-out;
    }

    :host(.show) {
      left: 0;
    }

    @media only screen and (min-width: 768px) {
      :host {
        left: 0;
      }
    }

   ${headerStyle} 
   ${contentStyle}

  </style>
  <div class="content">
    <a is="router-link" href="/" class="header" >
      <div class="header-logo">
        <img alt="logo" src="/img/white-logo.png">
      </div>
      <div class="header-name"> STOQKI ${getSubscription().toUpperCase()} </div>
    </a>
    <div class="list">
      ${items.map(item => itemTemplate(component, item)).join("")}
    </div>
  </div>
`;
