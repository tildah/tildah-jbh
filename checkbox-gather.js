export class CheckboxGather extends HTMLInputElement {

  constructor(...args) {
    super(...args);
    this.style.display = "none";
    this._list = {};
    this.watch();
  }

  watch() {
    const alterOn = this.dataset.from;
    if (alterOn) alterOn.split(";").forEach(event => {
      document.addEventListener(event, e => {
        this.alter(e);
      });
    });
  }

  get value() {
    return Object.keys(this._list).filter(item => this._list[item]);
  }

  alter(e) {
    const { value, checked } = e.detail;
    this._list[value] = checked;
  }

}

customElements.define("checkbox-gather", CheckboxGather, { extends: "input" });
