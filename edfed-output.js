import np from "nested-property";

export default class EDFEDOutput extends HTMLOutputElement {

  constructor() {
    super();
    this.render = this.render.bind(this);
  }

  connectedCallback() {
    this.outerClasses = this.className;
    this.watch();
    if (this.hasAttribute("hide")) this.style.display = "none";
  }

  render({ detail }) {
    this.value = this.getFinalValue(detail);
    this.dispatchUpdate();
  }

  dispatchUpdate() {
    const { changeCall } = this.dataset;
    if (!changeCall) return;
    const as = this.dataset.changeCallAs || "value";
    document.dispatchEvent(new CustomEvent(changeCall, {
      detail: { [as]: this.value },
    }));
  }

  getValue(detail) {
    const { alterKey } = this.dataset;
    return np.get(detail, alterKey);
  }

  getRoundValue(detail) {
    const value = this.getValue(detail) || parseFloat(this.dataset.empty);
    return value.toFixed(this.dataset.round);
  }

  getAddValue(detail) {
    const keys = this.dataset.keys.split(",");
    const values = keys.map(key => parseFloat(np.get(detail, key)));
    const value = values.reduce((t, it) => t + it, 0);
    return value.toFixed(this.dataset.round);
  }

  getSubtractValue(detail) {
    const a = np.get(detail, this.dataset.aKey);
    const b = np.get(detail, this.dataset.bKey);
    return (a - b).toFixed(this.dataset.round);
  }

  getReduceValue(detail) {
    const list = np.get(detail, this.dataset.alterKey || "");
    const operators = {
      "+": { fn: (a, b) => a + b, init: 0 },
      "*": { fn: (a, b) => a * b, init: 1 },
    };
    const { fn, init } = operators[this.dataset.operator];
    const { reduceKey } = this.dataset;
    const value = list.reduce(
      (t, it) => fn(t, parseFloat(np.get(it, reduceKey))),
      init,
    );
    return value.toFixed(this.dataset.round);
  }

  getRatioValue(detail) {
    const from = parseFloat(np.get(detail, this.dataset.fromKey));
    const rawRate = this.dataset.rate || np.get(detail, this.dataset.rateKey);
    const rate = parseFloat(rawRate);
    const value = from * rate / 100;
    return value.toFixed(this.dataset.round);
  }

  getDiscountValue(detail) {
    const from = parseFloat(np.get(detail, this.dataset.fromKey));
    const rate = parseFloat(np.get(detail, this.dataset.rateKey));
    const subbed = from * rate / 100;
    const value = from - subbed;
    return value.toFixed(this.dataset.round);
  }

  getCountValue(detail) {
    const list = np.get(detail, this.dataset.alterKey || "");
    const { countMode = "reset" } = this.dataset;
    const initialValues = {
      reset: 0,
      cumulate: isNaN(this.value) ? 0 : parseInt(this.value),
    };
    const initialValue = initialValues[countMode];
    const newVal = (list || []).length + initialValue;
    return newVal;
  }

  get valueMethod() {
    const methods = {
      round: this.getRoundValue,
      add: this.getAddValue,
      subtract: this.getSubtractValue,
      reduce: this.getReduceValue,
      percentage: this.getRatioValue,
      discount: this.getDiscountValue,
      count: this.getCountValue,
    };
    const method = methods[this.dataset.fn] || this.getValue;
    return method.bind(this);
  }

  getFinalValue(detail) {
    const { valueMethod } = this;
    const value = valueMethod(detail) || this.dataset.empty;
    const template = this.dataset.template || "%s";
    return template.replace(/%s/g, value);
  }

  watch() {
    const { alterOn } = this.dataset;
    if (alterOn) document.addEventListener(alterOn, this.render);
  }

  disconnectedCallback() {
    const { alterOn } = this.dataset;
    if (alterOn) document.removeEventListener(alterOn, this.render);
  }

}

customElements.define("edfed-output", EDFEDOutput, { extends: "output" });
