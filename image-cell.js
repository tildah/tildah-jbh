class ImageCell extends HTMLElement {

  get alts() {
    return {
      undefined: " ",
    };
  }

  get imgTag() {
    return /* html */ `
      <img src="${this.dataset.src}" class="products-list-image"/>
    `;
  }

  get template() {
    return /* html*/ `
      <style>
        .products-list-image {
          height: 40px;
        }
      </style>
      ${this.alts[this.dataset.src] || this.imgTag}
    `;
  }

  connectedCallback() {
    this.render();
  }

  render() {
    this.innerHTML = "";
    const templateEl = document.createElement("template");
    templateEl.innerHTML = this.template;
    this.appendChild(templateEl.content.cloneNode(true));
  }

}

customElements.define("image-cell", ImageCell);
