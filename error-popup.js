import "mdi-component/mdi-component.js";
import { mdiAlertCircle } from "@mdi/js";

class ErrorPopup extends HTMLElement {

  constructor(...args) {
    super(...args);
    this.attachShadow({ mode: "open" });
    this.show = this.show.bind(this);
    this.hide = this.hide.bind(this);
  }

  get template() {
    return /* html*/ `
      <style>
        :host {
          position: fixed;
          z-index: 3;
          padding: 8px 16px;
          box-sizing: border-box;
          background-color: rgba(220, 18, 33, 0.84);
          display: flex;
          align-items: center;
          width: 300px;
          left: -300px;
          color: white;
          transition: all .3s ease-in-out;
        }
        :host([visible])::after {
          content: attr(data-text);
        }
        :host([visible]) {
          left: 0;
        }
        .icon {
          margin: 8px;
        }
      </style>
      <mdi-component size="24px" path="${mdiAlertCircle}" class="icon">
      </mdi-component>
    `;
  }

  connectedCallback() {
    this.render();
    this.watch();
  }

  render() {
    this.shadowRoot.innerHTML = "";
    const templateEl = document.createElement("template");
    templateEl.innerHTML = this.template;
    this.shadowRoot.appendChild(templateEl.content.cloneNode(true));
  }

  hide() {
    this.removeAttribute("visible");
    this.dataset.text = "";
  }

  show(e) {
    this.setAttribute("visible", "true");
    this.dataset.text = e.detail.errorText || e.detail;
    setTimeout(this.hide, 2000);
  }

  watch() {
    document.addEventListener("error-popup:show", this.show);
  }

  disconnectedCallback() {
    document.removeEventListener("error-popup:show", this.show);
  }

}

customElements.define("error-popup", ErrorPopup);
