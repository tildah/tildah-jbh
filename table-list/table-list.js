import template from "./template.js";

class TableList extends HTMLElement {

  constructor(...args) {
    super(...args);
    this.shadow = this.attachShadow({ mode: "open" });
    if (!this.id) this.id = "table-list";
  }

  get columnsMap() {
    return this.getAttribute("columns").split(",").map(column => {
      const splitted = column.split(":");
      let [ key, label ] = splitted;
      label = label || key;
      return { key, label, column: true };
    });
  }

  get gridMap() {
    const gridSchema = this.getAttribute("grid-schema") || "";
    return gridSchema.split(";").map(cell => ({ key: cell, grid: true }));
  }

  get listProps() {
    const { gridMap, columnsMap: listProps } = this;
    gridMap.forEach(gridProp => {
      const column = listProps.find(col => col.key === gridProp.key);
      if (column) column.grid = true;
      else listProps.push(gridProp);
    });
    return listProps;
  }


  get relayedData() {
    const {
      loaderEvent,
      loadedEvent,
      pushFrom,
      fromKey,
      itemClkEvent,
      clkMode,
      linkTemp,
      linkProp,
      linkIs,
      itemMouseenterTo,
      itemMouseleaveTo,
      itemId,
      ...restData
    } = this.dataset;
    return Object.keys(restData).map(key => ({ key, value: restData[key] }));
  }


  connectedCallback() {
    this._list = [];
    this.render();
  }

  render() {
    this.shadow.innerHTML = "";
    const templateEl = document.createElement("template");
    templateEl.innerHTML = template(this);
    this.shadow.appendChild(templateEl.content.cloneNode(true));
  }

}

customElements.define("table-list", TableList);
