import "gen_components/map-list.js";
import "gen_components/cursor-event.js";
import { colors, textSizes } from "global-vars.js";

const mobileStyle = /* html*/ `
  @media only screen and (max-width: 768px) {
    .list {
      grid-template-columns:  auto;
      margin: 0;
      border-radius: 0;
    }

    .list-header {
      display: none;
    }

    .notgridded {
      display: none;
    }

    .notcolumn {
      display: block;
    }
    
    .grid-illustration {
      grid-area: ill;
      display: flex;
      align-items: center;
      justify-content: center;
    }

    .grid-title {
      grid-area: tlt;
      justify-content: flex-start;
    }

    .grid-subtitle {
      grid-area: sbt;
      color: ${colors.txtSecondary};
      justify-content: flex-start;
    }

    .grid-note {
      grid-area: nte;
      display: flex;
      align-items: center;
      justify-content: center;
    }

    .list-cell {
      border-bottom: none;
      position: relative;
      height: auto;
    }

    .list-item {
      display: grid;
      grid-template-columns: 56px auto 56px;
      grid-template-rows: 28px 28px;
      grid-template-areas: 
        "ill tlt nte"
        "ill sbt nte";
    }
  }
`;

function getGridClass(component, prop) {
  const classes = [
    "grid-illustration",
    "grid-title",
    "grid-subtitle",
    "grid-note",
  ];
  const index = component.gridMap.findIndex(cell => cell.key === prop.key);
  return index === -1 ? "notgridded" : classes[index];
}

function getColumnClass(prop) {
  return prop.column ? "" : "notcolumn";
}


function getDefaultCell(component, prop) {
  const gridClass = getGridClass(component, prop);
  const columnClass = getColumnClass(prop);
  const cellTemplate = document.createElement("template");
  cellTemplate.innerHTML = /* html */ `
      <div 
        class="list-cell ${gridClass} ${columnClass}"
        data-slot="${prop.key}"
      ></div>
    `;
  return cellTemplate;
}

function renderCell(component, prop) {
  const selector = `template[column="${prop.key}"]`;
  const cellEl = component.querySelector(selector) || getDefaultCell(component, prop);
  return cellEl.innerHTML;
}

export default component => /* html*/ `
  <style>
    .list {
      position: relative;
      background-color: #fff;
      color: ${colors.txtPrimary};
      display: grid;
      grid-template-columns: repeat(${component.columnsMap.length}, auto);
      box-shadow: 0px 2px 4px ${colors.shadow};
      border: 1px solid ${colors.border};
      border-radius: 16px;
      margin: 4px;
      width: 100%;
      box-sizing: border-box;
    }

    .list.heighted {
      overflow-y: scroll;
      max-height: ${component.dataset.height};
    }

    .list-header-cell {
      position: sticky;
      top: 0;
    }

    #${component.id}-map-list {
      display: contents;
    }

    .list-item, .list-header {
      font-size: ${textSizes.subheading};
      display: contents;
      padding: 0 8px;
      cursor: pointer;
    }

    .list-item {
      -webkit-animation-name: fadein; /* Safari 4.0 - 8.0 */
      -webkit-animation-duration: 0.5s; /* Safari 4.0 - 8.0 */
      animation-name: fadein;
      animation-duration: 0.5s ease-in-out;
    }

    @-webkit-keyframes fadein {
      from {opacity: 0;}
      to {opacity: 1;}
    }

    /* Standard syntax */
    @keyframes fadein {
      from {opacity: 0;}
      to {opacity: 1;}
    }

    a:nth-child(even) .list-item,
    cursor-event:nth-child(even) .list-item {
      background-color: rgba(0, 0, 0, .03);
    }

    .list-header-cell {
      color: ${colors.txtHint};
      text-align: center;
      padding: 8px;
      text-transform: capitalize;
      font-size: 14px;
      border-bottom: 1px solid ${colors.border};
    }

    .list-header-cell:first-child {
      border-top-left-radius: 16px;
    }

    .list-header-cell:last-child {
      border-top-right-radius: 16px;
    }

    .list-item-icon {
      color: ${colors.txtHint};
      margin: 0 12px;
    }

    .list-item.active {
      font-weight: bold;
    }

    .list-item.active .list-item-icon {
      color: ${colors.txtHint};
    }

    .item-link {
      color: inherit;
      text-decoration: inherit;
    }

    .list-cell {
      border-bottom: 1px solid rgba(0,0,0,.12);
      padding: 8px;
      display: flex;
      align-items: center;
      justify-content: center;
      height: 40px;
    }
    
    .notcolumn {
      display: none;
    }

    ${component.getAttribute("grid-schema") ? mobileStyle : ""}
  </style>
  <div class="list ${component.dataset.height ? "heighted" : ""}">
    <div class="list-header" >
      ${component.columnsMap.map(column => /* html */ `
        <div class="list-header-cell">${column.label}</div>
      `).join("")}
    </div>
    <template id="${component.id}-row-temp">
      <div class="list-item">
        ${component.listProps.map(prop => renderCell(component, prop)).join("")}
      </div>
    </template>
    <map-list id="${component.id}-map-list"
      data-loader-event="${component.dataset.loaderEvent || ""}"
      data-loaded-event="${component.dataset.loadedEvent || ""}"
      data-reset-on="${component.dataset.resetOn || ""}"
      data-push-from="${component.dataset.pushFrom || ""}"
      data-from-key="${component.dataset.fromKey || ""}"
      data-strew-on="${component.id}:strew"
      data-template="${component.id}-row-temp"
      data-clk-mode="${component.dataset.linkTemp ? "a" : "event"}"
      data-link-temp="${component.dataset.linkTemp || ""}"
      data-link-prop="${component.dataset.linkProp || "_id"}"
      data-link-is="router-link"
      data-item-mouseenter-to="${component.dataset.itemMouseenterTo || ""}"
      data-item-mouseleave-to="${component.dataset.itemMouseleaveTo || ""}"
      data-item-click-to="${component.dataset.itemClkEvent || ""}"
      data-item-id="${component.dataset.itemId || "_id"}"
      ${component.relayedData.map(d => `data-${d.key}="${d.value}"`).join("")}
    ></map-list>
  </div>
`;
