import { colors } from "global-vars.js";

class FormField extends HTMLElement {

  constructor(...args) {
    super(...args);
    this.attachShadow({ mode: "open" });
  }

  get display() {
    return this.hasAttribute("inline") ? "inline-flex" : "flex";
  }

  get compact() {
    return this.hasAttribute("compact");
  }

  get hostStyle() {

    return /* html */ `
      :host {
        padding: ${this.compact ? "0" : "2"}px;
        box-sizing: border-box;
        margin: ${this.compact ? "2" : "4"}px;
        display: ${this.display};
        flex-direction: column;
        position: relative;
      }

      :host label {
        font-size: 16px;
        text-transform: uppercase;
        color: #444444;
        margin: ${this.compact ? "0" : "4"}px;
      }
    `;
  }

  get style() {
    return /* html */ `
      <style>
        ${this.hostStyle}

        .container {
          border-radius: 4px;
          width: 100%;
          box-sizing: border-box;
          padding: 0;
          display: flex;
          align-items: stretch;
          background-color: #c9c9c9;
        }

        ::slotted(input), ::slotted(textarea), ::slotted(select) {
          color: ${colors.txtSecondary};
          border: none;
          outline: none;
          border-radius: 8px;
          padding: ${this.compact ? "4" : "8"}px;
          margin: 0;
          box-sizing: border-box;
          background-color: #c9c9c9 !important;
          width: 100%
        }
      </style>
    `;
  }

  get template() {
    return /* html*/ `
      ${this.style}
      ${this.hasAttribute("label") ? /* html */ `
        <label>${this.getAttribute("label")}</label>
      ` : ""}
      <div class="container">
        <slot></slot>
      </div>
    `;
  }

  connectedCallback() {
    this.render();
  }

  render() {
    this.shadowRoot.innerHTML = "";
    const templateEl = document.createElement("template");
    templateEl.innerHTML = this.template;
    this.shadowRoot.appendChild(templateEl.content.cloneNode(true));
  }

}

customElements.define("form-field", FormField);
