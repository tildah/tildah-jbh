export default () => /* html */ `
  <style>
    :host {
      font-size: 1.14rem;
      display: contents;
      padding: 0 8px;
      cursor: pointer;
    }

    :host{
      -webkit-animation-name: fadein; /* Safari 4.0 - 8.0 */
      -webkit-animation-duration: 0.5s; /* Safari 4.0 - 8.0 */
      animation-name: fadein;
      animation-duration: 0.5s ease-in-out;
    }

    @-webkit-keyframes fadein {
      from {opacity: 0;}
      to {opacity: 1;}
    }

    /* Standard syntax */
    @keyframes fadein {
      from {opacity: 0;}
      to {opacity: 1;}
    }
    
    @media only screen and (max-width: 768px) {
      :host {
        display: grid;
        grid-template-columns: 56px auto 56px;
        grid-template-rows: 28px 28px;
        grid-template-areas: 
          "ill tlt nte"
          "ill sbt nte";
      }
    }

  </style>
  <slot></slot>
`;
