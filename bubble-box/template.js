import "gen_components/event-guest.js";
import "dispatcher-button/dispatcher-button.js";
import "mdi-component/mdi-component.js";
import { mdiClose } from "@mdi/js";

export default component => /* html */ `
  <style>
      :host {
        position: absolute;
        z-index: 1;
      }
      .bubble {
        border: 1px solid #089bc7;
        box-shadow: 2px 2px 2px grey;
        border-radius: 9px;
        background-color: white;
        padding: 8px 16px;
        margin: 8px 0;
        width: 300px;
        max-width: 90%;
      }

      .close {
        color: white;
        background-color: black;
        border: 1px solid white;
        position: absolute;
        height: 20px;
        width: 20px;
        border-radius: 50%;
        display: flex;
        justify-content: center;
        align-items: center;
        font-size: 13px;
        cursor: pointer;
        top: 0;
        right: 15px;
      }

      .title {
        color: rgb(102, 142, 146);
        font-size: 16px;
        margin: 8px;
      }
    </style> 
    <event-guest showon="${component.id}:show" hideon="${component.id}:hide">
      <div class="bubble">
        <div class="title">${component.getAttribute("title-text")}</div>
        <dispatcher-button clk="${component.id}:hide" class="close">
          <mdi-component path="${mdiClose}" size="16"></mdi-component>
        </dispatcher-button>
        <slot></slot>
      </div>
    </event-guest>
`;
