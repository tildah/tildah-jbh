import template from "./template.js";

class BubbleBox extends HTMLElement {

  constructor(...args) {
    super(...args);
    this.attachShadow({ mode: "open" });
  }

  get style() {
    return /* html*/ `
      `;
  }

  connectedCallback() {
    this.render();
  }

  render() {
    this.shadowRoot.innerHTML = "";
    const templateEl = document.createElement("template");
    templateEl.innerHTML = template(this);
    this.shadowRoot.appendChild(templateEl.content.cloneNode(true));
  }

}

customElements.define("bubble-box", BubbleBox);
