import "gen_components/edfed-input.js";

class FileInput extends HTMLElement {

  constructor(...args) {
    super(...args);
    this.attachShadow({ mode: "open" });
    if (!this.id) throw new Error("File reader component must have an id");
    this.onChange = this.onChange.bind(this);
    this.onFileLoaded = this.onFileLoaded.bind(this);
    this.reader = new FileReader();
  }

  get template() {
    return /* html*/ `
      <style>
        :host {
        }
      </style>
      <input 
        is="edfed-input"
        type="file"
        accept="${this.dataset.accept}"
        change-call="${this.id}:changed"
      />
    `;
  }

  get readAs() {
    const matches = {
      "array-buffer": "ArrayBuffer",
      "binary-string": "BinaryString",
      text: "Text",
    };
    return matches[this.dataset.readAs] || "DataURL";
  }

  onChange(e) {
    const [ file ] = e.detail.target.files;
    this.reader.onload = this.onFileLoaded;
    this.reader[`readAs${this.readAs}`](file);
  }

  async onFileLoaded() {
    const source = await this.resize(this.reader.result);
    document.dispatchEvent(new CustomEvent(`${this.id}:loaded`, {
      detail: { source },
    }));
  }

  resize(base64Str) {
    return new Promise(resolve => {
      let img = new Image();
      img.src = base64Str;
      img.onload = () => {
        let canvas = document.createElement("canvas");
        let { width } = img;
        let { height } = img;
        const MAX_WIDTH = this.dataset.maxWidth || width;
        const MAX_HEIGHT = this.dataset.maxHeight || height;

        if (width > height) {
          if (width > MAX_WIDTH) {
            height *= MAX_WIDTH / width;
            width = MAX_WIDTH;
          }
        } else if (height > MAX_HEIGHT) {
          width *= MAX_HEIGHT / height;
          height = MAX_HEIGHT;
        }
        canvas.width = width;
        canvas.height = height;
        let ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0, width, height);
        resolve(canvas.toDataURL());
      };
    });
  }

  connectedCallback() {
    this.render();
    this.watch();
  }

  render() {
    this.shadowRoot.innerHTML = "";
    const templateEl = document.createElement("template");
    templateEl.innerHTML = this.template;
    this.shadowRoot.appendChild(templateEl.content.cloneNode(true));
  }

  watch() {
    document.addEventListener(`${this.id}:changed`, this.onChange);
  }

  disconnectedCallback() {
    document.removeEventListener(`${this.id}:changed`, this.onChange);
  }

}

customElements.define("file-input", FileInput);
