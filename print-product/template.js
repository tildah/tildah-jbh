import "gen_components/event-guest.js";
import "gen_components/edfed-span.js";
import "gen_components/edfed-img.js";
import "own_components/info-label.js";
import "own_components/barcode-draw.js";

const style = /* html*/ `
  <style>
    img {
      top: 0;
      left: 0;
    }

    info-label {
      flex-direction: row;
    }
  </style>
`;

const fieldsTemplates = {
  image: (field, component) => /* html */ `
    <img
      is="edfed-img"
      style="
        ${component.dataset.imgHeight ? `height: ${component.dataset.imgHeight}` : ""}
        ${component.dataset.imgWidth ? `width: ${component.dataset.imgWidth}` : ""}
      "
      event="${component.updateOnEvent}"
      key="image" />
  `,
  barcodeImage: (field, component) => /* html */ `
    <info-label label="${field.label}">
    </info-label>
    <barcode-draw
      data-event="${component.updateOnEvent}"
      data-key="barcode"
    >
    </barcode-draw>
  `,
  price: (field, component) => /* html */ `
    <info-label label="${field.label}:">
      <style>
        .price-discount:not(:empty) + .price-normal {
          text-decoration: line-through;
          font-size: 12px;
        }
      </style>
      <edfed-span
        event="${component.updateOnEvent}"
        key="discount"
        class="price-discount"
        empty=""
        empty-class="txt-hint"
      ></edfed-span>
      <edfed-span
        id="price"
        event="${component.updateOnEvent}"
        key="price"
        class="price-normal"
        empty="Ce champs n'est pas defini"
        empty-class="txt-hint"
      ></edfed-span>
      DT
    </info-label>
  `,
  default: (field, component) => /* html */ `
    <info-label label="${field.label}:">
      <edfed-span
        event="${component.updateOnEvent}"
        key="${field.name}"
        empty="${field.default || "Ce champs n'est pas defini"}"
        empty-class="txt-hint"
      ></edfed-span>
    </info-label>
  `,
};

function getFieldTemplate(field, component) {
  const templateFn = fieldsTemplates[field.name] || fieldsTemplates.default;
  return templateFn(field, component);
}

export default component => /* html */ `
  ${style}
  <event-guest showon="${component.updateOnEvent}" >
    ${component.fields.map(field => getFieldTemplate(field, component)).join("")}
  </event-guest>
`;
