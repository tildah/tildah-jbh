import template from "./template.js";

/*
* usage example1 :
* document.dispatchEvent(new CustomEvent("model:products:load-by-id",
* { detail: "{{ replace this by productId }}"}
* ))
* <print-product
*   data-rows="name;price"
*   data-update-on="model:products:loaded-by-id">
* </print-product>
*
* usage example2 :
* <print-product
*   id="print-product-example"
*   data-rows="name;price;ref">
* </print-product>
*
* <cursor-event
*   data-name="Product Name"
*   data-price=10
*   data-click-to="print-product-example:update"
*   >
*   <own-button
*     data-text="show product info">
*   </own-button>
* </cursor-event>
*/
class PrintProduct extends HTMLElement{

  constructor(...args) {
    super(...args);
    this.id = this.id || "print-product";
    this.getFieldObject = this.getFieldObject.bind(this);
    this.render();
  }

  render() {
    this.innerHTML = "";
    const templateEl = document.createElement("template");
    templateEl.innerHTML = template(this);
    this.appendChild(templateEl.content.cloneNode(true));
  }

  get updateOnEvent(){
    return this.dataset.updateOn || `${this.id}:update`;
  }

  get possibleFields (){
    return [
      { name: "image" },
      { name: "name", label: "Designation" },
      { name: "price", label: "Prix" },
      { name: "ref", label: "Référence" },
      { name: "barcode", label: "Code à barres" },
      { name: "barcodeImage", label: "Code à barres" },
      { name: "categsString", label: "Catégories" },
      { name: "qty", label: "Quantité", default: "0" },
    ];
  }

  getFieldObject(fieldName){
    const fieldProperties = this.possibleFields.find(f => f.name == fieldName);
    if (!fieldProperties)
      throw new Error("please choose a field with an appropriate name");
    return fieldProperties;
  }

  get fields() {
    const defaultFields = "name;price;ref;barcode;qty";
    const customFields = this.dataset.rows;

    // remove last semicolon
    const hasEndSemicolon = customFields.charAt(customFields.length - 1) == ";";
    if (hasEndSemicolon)
      throw new Error("please delete last semicolon");

    const fieldNames = customFields || defaultFields;
    const fields = fieldNames.split(";").map(name => this.getFieldObject(name));

    return fields;
  }

}

customElements.define("print-product", PrintProduct);
