import "own_components/user-corner/user-corner.js";
import "own_components/offline-patch/offline-patch.js";
import "dispatcher-button/dispatcher-button.js";
import "mdi-component/mdi-component.js";
import { colors } from "global-vars.js";
import { mdiMenu } from "@mdi/js";

export default () => /* html */ `
  <style>
    :host {
      display: flex;
      align-items: center;
      height: 40px;
      position: fixed;
      top: 0;
      width: 100%;
      box-sizing: border-box;
      background-color: white;
      box-shadow: 0px 0px 8px rgba(0, 0, 0, .54);
      z-index: 3;
    }
    .menu-icon {
      width: 56px;
      align-self: stretch;
      display: flex;
      justify-content: center;
      align-items: center;
      font-size: 32px;
      color: ${colors.txtSecondary};
    }
    .flex {
      flex: 1;
    }
  </style>
  <div class="menu-icon" side-bar-toggler>
    <mdi-component path="${mdiMenu}"></mdi-component>
  </div>
  <div class="flex"></div>
  <offline-patch></offline-patch>
  <user-corner></user-corner>
`;
