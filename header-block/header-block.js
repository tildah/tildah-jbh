import template from "./template.js";

class HeaderBlock extends HTMLElement {

  constructor(...args) {
    super(...args);
    this.attachShadow({ mode: "open" });
  }

  connectedCallback() {
    this.render();
  }

  render() {
    this.shadowRoot.innerHTML = "";
    const templateEl = document.createElement("template");
    templateEl.innerHTML = template();
    this.shadowRoot.appendChild(templateEl.content.cloneNode(true));
  }

}

customElements.define("header-block", HeaderBlock);
