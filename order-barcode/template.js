import "own_components/barcode-caller/barcode-caller.js";
import "own_components/barcode-scanner/barcode-scanner.js";
import "own_components/barcode-input.js";
import "gen_components/event-guest.js";
import { colors } from "global-vars.js";

export default () => /* html */ `
  <style>

    :host {
      border-top: 2px solid ${colors.main};  
      padding: 8px;
      width: 100%;
      box-sizing: border-box;
      display: flex;
      align-items: center;
      background-color: #FFF;
    }

    :host input {
      border: none;
      outline: none;
      flex: 1;
      height: 52px;
      font-size: 20px;
    }

    :host([data-type="back"]) {
      border-color: ${colors.red};
      border-width: 3px;
    }
    
    barcode-scanner {
      height: 300px;
    }

    @media only screen and (min-width: 768px) {
      barcode-scanner {
        height: 200px;
      }
    }

  </style>
  <event-guest
    start-visible
    style="display:contents"
    showon="barcode-scanner:camera:hide"
    hideon="barcode-scanner:camera:show"
  >
    <input
      is="barcode-input"
      event="model:expeditions:load-ordery"
      data-as="barcode"
      placeholder="Code à barres"
      cleanish
      autofocus
      data-focus-on="orders-create:update-items"
    >
    <barcode-caller data-scanner-id="barcode-scanner"></barcode-caller>
  </event-guest>
  <event-guest
    showon="barcode-scanner:camera:show"
    hideon="barcode-scanner:camera:hide"
    style="flex: 1" 
  >
    <barcode-scanner
      data-event="model:expeditions:load-ordery"
      data-as="barcode"
      data-mode="redo"
    ></barcode-scanner>
  </event-guest>
`;
