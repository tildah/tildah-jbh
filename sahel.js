/* eslint-disable max-lines-per-function */

export const dispatch = (event, details) => document.dispatchEvent(
  new CustomEvent(event, details),
);

export const showErrorPopUp = text => document.dispatchEvent(
  new CustomEvent("error-popup:show", { detail: text }),
);

export const redirect = href => document.dispatchEvent(
  new CustomEvent("router:redirect", { detail: { href } }),
);

export const define = compo => {

  const superClass = compo.inheritFrom || HTMLElement;
  const listeners = {};

  class CustomElement extends superClass {

    static formAssociated = compo.formAssociated;

    constructor() {
      super();
      this.attachShadow({ mode: "open" });
      if (compo.formAssociated) this.internals_ = this.attachInternals();
      const { listeners: compoListeners = () => [] } = compo;
      this.listeners = compoListeners({ ...compo, element: this });
    }

    connectedCallback() {
      const templateEl = document.createElement("template");
      templateEl.innerHTML = compo.template({ ...compo, element: this });
      this.shadowRoot.appendChild(templateEl.content.cloneNode(true));
      this.watch();
    }

    watch() {
      this.listeners.forEach(listener => {
        const { target = document, eventFrom, event } = listener;
        const eventName = event || compo.getAttribute(eventFrom);
        listeners[eventName] = listener.fn({ ...compo, element: this });
        target.addEventListener(event, listeners[eventName]);
      });
    }

    disconnectedCallback() {
      this.listeners.forEach(listener => {
        const { target = document, eventFrom, event } = listener;
        const eventName = event || compo.getAttribute(eventFrom);
        target.addEventListener(event, listeners[eventName]);
      });
    }
  }

  const options = { ...(compo.is ? { extends: compo.is } : {}) };
  customElements.define(compo.as, CustomElement, options);
};
