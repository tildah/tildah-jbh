import { colors } from "global-vars.js";

export default component => /* html */ `
  <style>
    :host {
      max-width: 100%;
      position: relative;
    }
    :host(.price-box) {
      min-width: 216px;
    }
    .container {
      background-color: white;
      box-shadow: 0 0 8px ${colors.shadow};
      padding: 16px 24px;
      margin: 8px;
      box-sizing: border-box;
    }
    .title {
      font-size: 16px;
      color: ${colors.txtPrimary};
      margin: 4px 0;
    }
    .content {
      display: flex;
      flex-direction: column;
      align-items: flex-start;
    }
    ::slotted(.section) {
      margin: 8px;
      max-width: 100%;
      display: flex;
      justify-content: center;
    }

    ::slotted(.link) {
      position: absolute;
      display: block;
      top: 12px;
      right: 12px;
    }
  </style>
  <div class="container">
    <div class="title">${component.dataset.title || ""}</div>
    <div class="content">
      <slot></slot>
    </div>
  </div>
`;
