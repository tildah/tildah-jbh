import "mdi-component/mdi-component.js";
import "dispatcher-button/dispatcher-button.js";
import JsBarcode from "jsbarcode";
import { mdiPrinter } from "@mdi/js";

class BarcodeDraw extends HTMLElement {

  constructor() {
    super();
    this.attachShadow({ mode: "open" });
    this.draw = this.draw.bind(this);
    this.print = this.print.bind(this);
  }

  get event() {
    return this.dataset.event;
  }

  get barcodeEl() {
    return this.shadowRoot.querySelector("#barcode-draw");
  }

  get template() {
    return /* html*/ `
      <style>
        .print-icon {
          cursor: pointer;
        }
      </style>
      <svg id="barcode-draw"></svg>
    <dispatcher-button class="print-icon" clk="barcode-draw:print">
      <mdi-component size="24px" path="${mdiPrinter}"></mdi-component>
    </dispatcher-button>
    `;
  }

  connectedCallback() {
    this.render();
    this.watch();
  }

  render() {
    this.shadowRoot.innerHTML = "";
    const templateEl = document.createElement("template");
    templateEl.innerHTML = this.template;
    this.shadowRoot.appendChild(templateEl.content.cloneNode(true));
  }

  get barcode() {
    const { key } = this.dataset;
    return this.product[key];
  }

  draw(e) {
    this.product = e.detail;
    JsBarcode(this.barcodeEl, this.barcode);
  }

  selectEndpoint(direction, device) {
    const endpoint = device.configuration
      .interfaces[0]
      .alternate
      .endpoints.find(ep => ep.direction == direction);

    if (endpoint == null)
      throw new Error(`Endpoint ${direction} not found in device interface.`);
    return endpoint;
  }

  get printedStr() {
    const { barcode } = this;
    const printData = [
      "\x1b\x61\x01", // align center
      "\x1d\x77", // width base
      barcode.length <= 10 ? "\x03" : "\x02",
      "\x1d\x68\x64", // height: Default
      "\x1d\x66\x01", // font: B
      "\x1d\x48\x02", // position: Chars Below
      "\x1d\x6b\x04", // type: CODE39
      barcode, // code
      "\x00", // closure
      "\x1b\x61\x00", // align left
      "\x1b!\x32",
      `\n${this.product.name}`,
      "\x1b!\x00",
      `\n${this.product.ref}`,
      "\x1b!\x08",
      `\n\n${(this.product.discount || this.product.price).padStart(16)} DT`,
      "\n\n\n\n\n\x1d\x56\x00", // cut
    ];
    return printData.join("");
  }

  async print() {
    const device = await navigator.usb.requestDevice({ filters: [] });
    await device.open();
    await device.selectConfiguration(1);
    const { interfaceNumber } = device.configuration.interfaces[0];
    await device.claimInterface(interfaceNumber);
    const str = this.printedStr;
    const bytes = str.split("").map(char => char.charCodeAt(0));
    const data = new Uint8Array(bytes);
    const endpoint = this.selectEndpoint("out", device);
    await device.transferOut(endpoint.endpointNumber, data);
    await device.close();
  }

  watch() {
    if (this.event) document.addEventListener(this.event, this.draw);
    document.addEventListener("barcode-draw:print", this.print);
  }

  disconnectedCallback() {
    if (this.event) document.removeEventListener(this.event, this.draw);
    document.removeEventListener("barcode-draw:print", this.print);
  }

}

customElements.define("barcode-draw", BarcodeDraw);
