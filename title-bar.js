import "dispatcher-button/dispatcher-button.js";
import "mdi-component/mdi-component.js";
import "own_components/lang-span.js";
import { colors, textSizes } from "global-vars.js";
import { mdiArrowLeft } from "@mdi/js";

class TitleBar extends HTMLElement {

  constructor(...args) {
    super(...args);
    this.attachShadow({ mode: "open" });
  }

  get style() {
    return /* html */ `
      <style>
        :host {
          display: flex;
          align-items: center;
          padding: 8px;
          height: 24px;
          text-transform: capitalize;
          font-size: ${textSizes.subheading};
          color: ${colors.txtSecondary};
          border-bottom: 1px solid ${colors.border};
          position: relative;
        }
        .flex {
          flex: 1;
        }

        .back-icon {
          margin: 0 8px;
          cursor: pointer;
        }
      </style>
    `;
  }

  get template() {
    return /* html*/ `
      ${this.style}
      <dispatcher-button clk="router:back">
        <mdi-component size="24px" path="${mdiArrowLeft}" class="back-icon">
        </mdi-component>
      </dispatcher-button>
      <lang-span 
        class="title"
        data-event="${this.dataset.event || ""}"
        data-key="${this.dataset.key || ""}"
        data-template="${this.dataset.template || "%s"}"
        data-text="${this.dataset.text || ""}"
      ></lang-span>
      <slot name="title-supplement"></slot>
      <div class="flex"></div>
      <slot></slot>
    `;
  }

  connectedCallback() {
    this.render();
  }

  render() {
    this.shadowRoot.innerHTML = "";
    const templateEl = document.createElement("template");
    templateEl.innerHTML = this.template;
    this.shadowRoot.appendChild(templateEl.content.cloneNode(true));
  }

}

customElements.define("title-bar", TitleBar);
