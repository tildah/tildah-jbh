class InfoLabel extends HTMLElement {

  constructor(...args) {
    super(...args);
    this.attachShadow({ mode: "open" });
  }

  get display() {
    return this.hasAttribute("inline") ? "inline-flex" : "flex";
  }

  get style() {
    return /* html */ ` <style>
        :host {
          padding: 8px;
          box-sizing: border-box;
          display: ${this.display};
          flex-direction: column;
          align-items: flex-start;
          position: relative;
        }
        .content {
          padding: 8px;
          font-size: 14px;
        }
        :host .label {
          font-size: 16px;
          color: #38bec5;
          font-weight: bold;
          text-transform: capitalize;
          margin: 4px;
        }

        @media only screen and (min-width: 768px) {
          :host {
            flex-direction: row;
            align-items: center;
          }
        }
      </style> `;
  }

  get template() {
    return /* html*/ `
      ${this.style}
      <span class="label">${this.getAttribute("label")}</span>
      <span class="content">
        <slot></slot>
      </span>
    `;
  }

  connectedCallback() {
    this.render();
  }

  render() {
    this.shadowRoot.innerHTML = "";
    const templateEl = document.createElement("template");
    templateEl.innerHTML = this.template;
    this.shadowRoot.appendChild(templateEl.content.cloneNode(true));
  }

}

customElements.define("info-label", InfoLabel);
