import Ambassador from "services/ambassador.js";

let _session = { visa: [] };

export default class LoginManager extends Ambassador {

  static get session() {
    return _session;
  }

  static set session(newSession) {
    _session = newSession;
  }

  static get isLoaded() {
    return this.session._id;
  }

  static get isAuthed() {
    return localStorage.getItem("token") && this.isLoaded;
  }

  static get module() {
    return "users";
  }

  static async load() {
    try {
      this.session = await this.request("me") || {};
      this.session.visa = await this.request("me", { module: "visa" }) || [];
    } catch (e) {
      console.log(e);
      this.session = { visa: [] };
    }
    if (!this.session._id) this.softLogout();
  }

  static can(access) {
    return (this.session.visa || []).includes(access);
  }

  static async getData() {
    if (!this.isLoaded) await this.load(this);
    document.dispatchEvent(new CustomEvent("auth:user-loaded", {
      detail: this.session,
    }));
    return this.session;
  }

  static rejectUnauthed() {
    if (!this.isAuthed) throw { code: 401, msg: "Login required" };
  }

  static filterAccess(access) {
    if (!access) return;
    const accessArray = [ access ].flat();
    if (accessArray.every(a => !this.can(a)))
      throw { code: 403, msg: "Access forbidden" };
  }

  static async disguise(_id) {
    const response = await this.request(`disguise/${_id}`);
    if (!response) return;
    localStorage.setItem("disguise", response.token);
    await this.load();
    document.dispatchEvent(new CustomEvent("auth:user-changed"));
    document.dispatchEvent(new CustomEvent("router:redirect", {
      detail: { href: "/" },
    }));
  }

  static async login(body) {
    const response = await this.request("login", {
      method: "POST",
      body,
    });
    if (!response) return;
    localStorage.setItem("token", response.token);
    await this.load();
    document.dispatchEvent(new CustomEvent("router:redirect", {
      detail: { href: "/" },
    }));
  }

  static async softLogout() {
    localStorage.removeItem("token");
    localStorage.removeItem("disguise");
    const cache = await caches.open(process.env.CACHE_NAME);
    cache.delete(`${process.env.API_URL}/auth/me`);
    cache.delete(`${process.env.API_URL}/visa/me`);
  }

  static async logout() {
    this.softLogout();
    window.location = "/login";
  }

  static async restartSession() {
    const persistentRoles = [ "store_owner", "admin" ];
    const fromOutside = !document.referrer.startsWith(window.location.origin);
    const isVolatile = !persistentRoles.includes(LoginManager.session.role);
    if (isVolatile && fromOutside) LoginManager.logout();
  }
}

document.addEventListener("auth:login", e => {
  LoginManager.login(e.detail);
});

document.addEventListener("auth:disguise", e => {
  LoginManager.disguise(e.detail._id);
});

document.addEventListener("auth:logout", e => {
  LoginManager.logout(e);
});

document.addEventListener("auth:reject-unauthed", e => {
  LoginManager.rejectUnauthed(e);
});

document.addEventListener("auth:filter-access", e => {
  LoginManager.filterAccess(e.detail);
});

document.addEventListener("auth:load-user", () => {
  LoginManager.getData();
});

document.addEventListener("auth:reload-user", async () => {
  await LoginManager.load();
  document.dispatchEvent(new CustomEvent("auth:user-reloaded"));
});

