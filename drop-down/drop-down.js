import template from "./template.js";

class DropDown extends HTMLElement {

  constructor(...args) {
    super(...args);
    this.attachShadow({ mode: "open" });
    if (!this.id) throw new Error("`drop-down` should have id");
    this.onClickWindow = this.onClickWindow.bind(this);
  }

  connectedCallback() {
    this.render();
    this.watch();
  }

  render() {
    this.shadowRoot.innerHTML = "";
    const templateEl = document.createElement("template");
    templateEl.innerHTML = template(this);
    this.shadowRoot.appendChild(templateEl.content.cloneNode(true));
  }

  onClickWindow() {
    document.dispatchEvent(new CustomEvent(`${this.id}:hide`));
  }

  get watchers() {
    return [
      { event: "click", listener: this.onClickWindow, target: document },
    ];
  }

  watch() {
    this.watchers.forEach(w => {
      (w.target || document).addEventListener(w.event, w.listener);
    });
  }

  disconnectedCallback() {
    this.watchers.forEach(w => {
      (w.target || document).removeEventListener(w.event, w.listener);
    });
  }

}

customElements.define("drop-down", DropDown);
