import "../cursor-event.js";
import "../event-guest.js";

export default component => /* html */ `
  <style>
    :host {
      position: relative;
    }

    .menu {
      position: absolute;
      right: 0;
      z-index: 5;
      width: max-content;
      background-color: #FFF;
      box-shadow: 0px 4px 12px rgba(0,0,0,.3);
    }
  </style>
  <cursor-event data-click-to="${component.id}:show">
    <slot name="toggle"></slot>
  </cursor-event>
  <event-guest
    showon="${component.id}:show"
    hideon="${component.id}:hide"
  >
    <div class="menu">
      <slot name="menu"></slot>
    </div>
  </event-guest>
`;
