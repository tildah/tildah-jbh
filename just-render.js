export default class JustRender extends HTMLElement {
  constructor(...args) {
    super(...args);
    this.attachShadow({ mode: "open" });
  }

  connectedCallback() {
    this.render();
  }

  render() {
    this.shadowRoot.innerHTML = "";
    const templateEl = document.createElement("template");
    templateEl.innerHTML = this.template();
    this.shadowRoot.appendChild(templateEl.content.cloneNode(true));
  }
}

