export default class EDFEDForm extends HTMLFormElement {

  constructor(...args) {
    super(...args);
    this.submit = this.submit.bind(this);
    this.getcheckboxValue = this.getcheckboxValue.bind(this);
    this.addEventListener("submit", this.submit);
    this.addEventListener("keyup", this.handleKeyup);
    this.watch();
  }

  watch() {
    const submitOn = this.getAttribute("submit-on");
    if (!submitOn) return;
    document.addEventListener(submitOn, this.submit);
  }

  disconnectedCallback() {
    const submitOn = this.getAttribute("submit-on");
    if (!submitOn) return;
    document.removeEventListener(submitOn, this.submit);
  }

  getElementValue(element, res) {
    const key = element.getAttribute("name");
    res[key] = element.value;
  }

  getcheckboxValue(element, res) {
    const key = element.getAttribute("name");
    if (element.hasAttribute("array")) {
      res[key] = res[key] || [];
      if (element.checked) res[key].push(element.value);
    } else {
      res[key] = element.checked;
    }
  }

  get data() {
    const res = {};
    const children = this.elements;
    Array.from(children).forEach(element => {
      const elName = element.getAttribute("name");
      if (!elName || !element.value) return;
      const fn = this[`get${element.type}Value`] || this.getElementValue;
      fn(element, res);
    });
    return res;
  }

  handleKeyup(e) {
    if (e.keyCode === 13 && !e.shiftKey && this.dataset.submitEnter)
      this.submit(e);
  }

  submit(e) {
    document.dispatchEvent(new CustomEvent(this.getAttribute("submit-to"), {
      detail: this.data,
    }));
    e.preventDefault();
  }

}

customElements.define("edfed-form", EDFEDForm, { extends: "form" });
