import template from "./template.js";
import LoginManager from "app_modules/login/manager.js";

class ProductsTable extends HTMLElement {

  constructor(...args) {
    super(...args);
    if (!this.id) this.id = "products_list";
    this.attachShadow({ mode: "open" });
    this.filterList = this.filterList.bind(this);
    this.filterCategs = this.filterCategs.bind(this);
    this.onItemClk = this.onItemClk.bind(this);
  }

  get listColumns() {
    const { fieldums } = LoginManager.session.branch.store;
    const tablans = fieldums.filter(f => f.tablan).map(f => f.name);
    return this.dataset.listColumns || [
      "image:Photo",
      "ref",
      "name:Designation",
      ...tablans,
      "qty:Qté",
      "price:Prix U (dt)",
      "value:Valeur",
    ].join(",");
  }

  get defaultGrid() {
    const map = [ "image", "name", "ref", "qty" ];
    return map.join(";");
  }

  get gridSchema() {
    const { gridSchema, list_columns: listColumns } = this.dataset;
    if (gridSchema) return gridSchema;
    if (!listColumns && !gridSchema) return this.defaultGrid;
    return "";
  }

  get tableId() {
    return `${this.id}_table`;
  }

  connectedCallback() {
    this.templates = this.innerHTML;
    this.searchText = "";
    this.selectedCateg = "*";
    this.selectedGroup = null;
    this.watch();
    this.render();
  }

  get relayedData() {
    const {
      loadedEvent,
      listColumns,
      noselect,
      ...relayedData
    } = this.dataset;
    return Object.keys(relayedData).map(key => ({
      key, value: relayedData[key],
    }));
  }

  render() {
    this.shadowRoot.innerHTML = "";
    const templateEl = document.createElement("template");
    templateEl.innerHTML = template(this);
    this.shadowRoot.appendChild(templateEl.content.cloneNode(true));
  }

  updateList() {
    document.dispatchEvent(new CustomEvent(this.listUpedEv, {
      detail: this.list,
    }));
  }

  filterList(e) {
    if (e.detail.value.length < 3 && e.detail.value.length > 0) return;
    this.searchText = e.detail.value.toLowerCase();
    this.reloadData();
  }

  filterCategs(e) {
    this.selectedCateg = e.detail.value;
    this.reloadData();
  }

  onItemClk(e) {
    if (!LoginManager.can("GET OWN PRODUCT") || this.dataset.noselect) return;
    const href = `/products/${e.detail.productId}`;
    document.dispatchEvent(new CustomEvent("router:redirect", {
      detail: { href },
    }));
  }

  reloadData() {
    const query = {};
    if (this.searchText.length) query.search = this.searchText;
    if (this.selectedCateg !== "*") query.category = this.selectedCateg;
    document.dispatchEvent(new CustomEvent("model:expeditions:search", {
      detail: query,
    }));
  }

  get watchers() {
    return [
      { event: `${this.tableId}:item_selected`, listener: this.onItemClk },
    ];
  }

  watch() {
    this.watchers.forEach(watcher => {
      document.addEventListener(watcher.event, watcher.listener);
    });
  }

  disconnectedCallback() {
    this.watchers.forEach(watcher => {
      document.removeEventListener(watcher.event, watcher.listener);
    });
  }

}

customElements.define("products-table", ProductsTable);
