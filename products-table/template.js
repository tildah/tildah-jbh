import "gen_components/event-guest.js";
import "gen_components/edfed-select.js";
import "gen_components/edfed-form.js";
import "own_components/barcode-input.js";
import "own_components/table-list/table-list.js";
import "own_components/form-field.js";
import "own_components/own-link/own-link.js";
import "own_components/image-cell.js";
import "own_components/flex-box.js";
import "own_components/barcode-popup/barcode-popup.js";
import "own_components/barcode-caller/barcode-caller.js";
import "gen_components/cursor-event.js";
import "gen_components/auto-dispatcher.js";
import "gen_components/event-event.js";
import "gen_components/drop-down/drop-down.js";
import "mdi-component/mdi-component.js";
import LoginManager from "app_modules/login/manager.js";
import { colors } from "global-vars.js";

export default component => /* html */ `
  <style>
    .search-form {
      background-color: #c9c9c9; 
      width: max-content;
      display: flex;
      align-items: center; 
      margin: 0;
    }

    .search-form form-field:not(:first-of-type) {
      border-left: 1px solid ${colors.txtHint};
    }

    .barcode-caller {
      display: flex;
      align-items: center;
    }
    
    @media only screen and (min-width: 768px) {
      .search-form {
        margin: 8px;
        border-radius: 4px;
      }
    }
  </style>
  <auto-dispatcher 
    data-to="model:expeditions:fetch"
    data-action="${component.dataset.action || ""}"
  ></auto-dispatcher>
  <event-event
    data-on="model:expeditions:fetched:${component.dataset.action || ""}"
    data-to="${component.id}:search"
  ></event-event>
  <barcode-popup
    data-scanner-id="barcode-scanner"
    data-to="${component.id}:barcode:scanned">
  </barcode-popup>
  <flex-box train>
    <div class="search-form">
      ${LoginManager.can("CREATE BRANCH") ? /* html */ `
        <form is="edfed-form" id="${component.id}-branches-form"
          submit-on="${component.id}:filter:branch"
          submit-to="model:expeditions:fetch"
          data-action=""
        ></form>
        <form-field>
          <select 
            is="edfed-select" id="${component.id}-branches-select"
            change-call="${component.id}:filter:branch"
            name="branch"
            form="${component.id}-branches-form"
          >
            <option value="${LoginManager.session.branchId}" selected>
              ${LoginManager.session.branch.name}
            </option>
          </select>
          <template id="${component.id}-branches-list-template">
            <option data-patch="_id:value" data-slot="name"></option>
          </template>
          <map-list id="clients_list"
            data-loader-event="model:branches:load"
            data-loaded-event="model:branches:loaded"
            data-template="${component.id}-branches-list-template"
            data-host="${component.id}-branches-select"
            data-mode="append"
            data-type="child"
        ></map-list>
        </form-field>
      ` : ""}    
      <form is="edfed-form" id="${component.id}-filter-form"
        class="search-form"
        submit-on="${component.id}:search"
        submit-to="model:expeditions:search"></form>


        <form-field>
          <input is="barcode-input"
            placeholder="Filtrer par ref, code a ..., des..."
            alter-on="${component.id}:barcode:scanned"
            alter-key="value"
            change-call="${component.id}:search"
            event="${component.id}:search"
            name="search"
           form="${component.id}-filter-form"
          >
          <barcode-caller data-scanner-id="barcode-scanner" class="barcode-caller"></barcode-caller>
        </form-field>
        <form-field>
          <select is="edfed-select"
            name="category"
            change-call="${component.id}:search"
            form="${component.id}-filter-form"
          >
            <option value="" selected>Toutes les catégories</option>
            ${LoginManager.session.branch.store.categories.map(category => /* html */ `
              <option value="${category._id}" >${category.name}</option>
            `)}
          </select>
        </form-field>
    </div>
    <div flex-fill></div>
    <slot name="tools"></slot>
  </flex-box>
  <table-list
    data-loaded-event="model:expeditions:searched"
    columns="${component.listColumns}"
    id="${component.tableId}"
    grid-schema="${component.gridSchema}"
    data-item-clk-event="${component.tableId}:item_selected"
    ${component.relayedData.map(d => `data-${d.key}="${d.value}"`).join("")}
    ${component.dataset.noselect ? "data-noselect='true'" : ""}
  >
    <template column="image">
      <div class="list-cell grid-illustration">
        <image-cell data-patch="image:data-src"></image-cell>
      </div>
    </template>
    <template column="refName">
      <div class="list-cell grid-title notcolumn">
        <span data-slot="ref"></span> - <span data-slot="name"></span>
      </div>
    </template>
    <template column="price">
      <div class="list-cell notgridded">
        <style>
          .price-cell-discount:not(:empty) + .price-cell-normal {
            text-decoration: line-through;
            font-size: 12px;
          }
        </style> 
        <flex-box row-center>
          <div class="price-cell-discount" data-slot="discount"></div>
          <div class="price-cell-normal" data-slot="price"></div>
        </flex-box>
      </div>
    </template>
    ${component.templates}
  </table-list>
`;
